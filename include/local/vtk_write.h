#ifndef VTK_WRITE_H
#define VTK_WRITE_H

#include <vtkPointData.h>
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkTriangle.h>
#include <vtkPolyDataMapper.h>
#include <vtkLine.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkDoubleArray.h>

#include "vectors.h"
#include "print.h"

void vtk_3d_graph(System *sys, Logger *logger);
void vtk_internal_graph(System *sys, Logger *logger);
void vtk_center_vectors(System *sys, Logger *logger);
void vtk_points(System *sys, Logger *logger);
void vtk_triangle(System *sys, Logger *logger, cx_mat In);
void vtk_mesh(System *sys, Logger *logger);
void vtk_edges(System *sys, Logger *logger);

#endif
