#ifndef VECTOR_H
#define VECTOR_H

#include <armadillo>

#include "edge.h"

using namespace arma;

double vector_norm(Node *vect);
Node vector_cross(Node *vec1, Node *vec2);
Node vector_mul_add(Node r, double Lx, Node *rx);
double vector_dot(Node pm, Node pn);
Node vector_sub(Node *vec1, Node *vec2);
double vector_inner_sum(vec V);

#endif
