#ifndef BOOL_H
#define BOOL_H

#include "edge.h"

bool has_node_been_visited(vector<int> &visited_nodes, int id);
bool is_triangle_connected_to_node(Triangle *edge_triangle, Vertex *node);
bool is_terminal_triangle(Triangle *tri);
bool is_edge_list_zero(int numEdge);
bool does_edge_exist(vector<Edge *> edge, int numEdge, int id1, int id2);
bool is_center_negative(int direct);
bool is_new_edge_equal_to_old_edge(Edge *edge_old, Edge *edge_new);
bool is_triangle_reached(vector<Triangle *> &tri_added, Triangle *tri);
bool is_current_triangle_pos(Edge *edge, Triangle *tri);
bool is_current_triangle_neg(Edge *edge, Triangle *tri);

#endif
