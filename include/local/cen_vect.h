#ifndef CENTERVECTOR_H
#define CENTERVECTOR_H

#include "vertex.h"

class CenterVector
{
    public:
        int id;
        int direct;
        Node vector;
        Triangle *tri;

        inline CenterVector();
        inline ~CenterVector();
};

// 0 means not assigned, 1 is pointing to center, 2 is pointing away from center
inline CenterVector::CenterVector()
{
    direct = DIRECT_EMPTY;
    // *tri = new Triangle;
    // tri = NULL;
}

inline CenterVector::~CenterVector() {}

#endif
