#ifndef VERTEX_H
#define VERTEX_H

#include "struct.h"
#include "graph.h"

typedef std::pair<Triangle *, int> myPair;

class Vertex
{
    public:
        int id;
        int type;
        int divid_num;
        double curr_vector[2];
        Node *node;
        std::vector<int> tri;
        Graph *g;

        inline Vertex();
        inline ~Vertex();
};

inline Vertex::Vertex()
{
    node = new Node();
    divid_num = 0;
    curr_vector[0] = 0.0;
    curr_vector[1] = 0.0;
    type = TYPE_INTERNAL;
}

inline Vertex::~Vertex() {}

#endif
