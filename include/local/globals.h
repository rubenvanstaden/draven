#ifndef GLOBALS_H
#define GLOBALS_H

/* Version info */
#define DEVELOPERS "Ruben van Staden"
#define HACKER "1h3d*n"
#define VERSION "David-Blaine v1.0"
#define DATE "21 April 2017"

/* Constants */
#define PI 3.14159265358979323846
#define MU 1.256637061e-6
#define EPS 8.85418782E-12
#define PHI 2.067833667E-15
#define DIM 1e-6

/* Geometry */
#define TYPE_EMPTY 0
#define TYPE_INTERNAL 1
#define TYPE_BOUNDARY 2
#define TYPE_TERMINAL 3

// Center vector direction
#define DIRECT_EMPTY 0
#define DIRECT_POS 1
#define DIRECT_NEG 2

/* Input file formats */
#define INPUT_FORMAT_NETGEN 0
#define INPUT_FORMAT_GMSH 1
#define INPUT_FORMAT_NASTRAN 2

/* Gmsh file properties */
#define PROPERTY_FREQ 1
#define PROPERTY_MATERIAL 2
#define PROPERTY_TERMINAL 3
#define PROPERTY_HOLE 4

#endif
