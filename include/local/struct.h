#ifndef STRUCT_H
#define STRUCT_H

#include <vector>
#include <iostream>
#include <armadillo>
#include <stddef.h>
#include <iomanip>
#include <typeinfo>

#include "globals.h"
#include "args.h"
#include "logger.h"

/**************************************************************
 * Hacker: 1h3d*n
 * For: Volundr
 * Docs: Main
 * Date: 19 April 2017
 *
 * This is the top header file in the herarchy. It includes the
 * main system struture of the program that holds all the main
 * info that will be used in the program.
 **************************************************************/

typedef std::map<std::string, std::string> mapFiles;
typedef std::map<std::string, std::string> mapTerms;


class Vertex;
class Triangle;
class Edge;

struct Node {
    int id;
    double x;
    double y;
    double z;
};

struct Terminal {
    int id;
    Node *node[2];
};

struct System
{
    bool debug;

    int numTri;
    int numVert;
    int numEdge;
    int numTerm;
    int numCent;

    double omega;

    arma::sp_cx_mat L;
    arma::sp_cx_mat R;
    arma::sp_cx_mat Z;
    arma::sp_cx_mat M;

    arma::mat initMap;
    arma::mat conMap;
    arma::mat boundaryMap;
    arma::mat edgeIdMap;

    vector<Vertex *> vecVert;
    vector<Triangle *> vecTri;
    vector<Edge *> vecEdge;

    mapFiles m_files;
    mapTerms m_terms;
};
// }

#endif
