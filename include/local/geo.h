#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "connect.h"
#include "vectors.h"
#include "print.h"

void construct_geometry(System *sys, Logger *logger);
void detect_boundary_edges(System *sys, Logger *logger);
void detect_boundary_vertex(System *sys, Logger *logger);
void create_triangle_area(System *sys, Logger *logger);
void create_triangle_center_nodes(System *sys, Logger *logger);
void create_triangle_center_vectors(System *sys, Logger *logger);
int get_center_direction(System *sys, int i);
void print_cen_vects(System *sys, Triangle tri);

#endif
