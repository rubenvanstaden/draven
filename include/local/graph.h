#ifndef GRAPH_H
#define GRAPH_H

// http://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-using-priority_queue-stl/

// Program to find Dijkstra's shortest path using priority_queue in STL
#include <bits/stdc++.h>
#include <stddef.h>
#include "struct.h"

# define INF 0x3f3f3f3f

// iPair ==> Integer Pair
typedef std::pair<int, int> iPair;

// This class represents a directed graph using adjacency list representation
class Graph
{
    public:
        int V;

        // In a weighted graph, we need to store vertex
        // and weight pair for every edge
        list<iPair> *adj;
        std::map<int, Triangle *> vprop;

        int id;
        int type;

        Graph(int V);
        ~Graph();

        void add_edge(int u, int v, int w);
        void shortest_path(int s);
        // void printPath(int parent[], int j);
        // int printSolution(int dist[], int n, int parent[]);
};

#endif
