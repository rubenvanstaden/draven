#ifndef GRAPH_BOOST_H
#define GRAPH_BOOST_H

typedef std::pair <double, double> cNode;

struct VertexProperty
{
    int Id;
    int Type;
    int Direction;
    cNode Pos;
    bool Source;
    Triangle *Tri;
};

void generate_graph(Vertex *vert, std::vector<VertexProperty> &graph_nodes);
void directed_generated_graph(Vertex *vert, std::vector<VertexProperty> &graph_nodes);

#endif
