#ifndef TOOLS_H
#define TOOLS_H

#include "struct.h"
#include "logger.h"

void file_system(System *sys, std::string testString, Logger *logger);
void print_info(System *sys);
void set_options(System *sys, Logger *logger);
void write_time(int day);
void read_quote(int num);
void create_quote();

#endif
