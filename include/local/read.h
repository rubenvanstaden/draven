#ifndef READ_H
#define READ_H

#include "connect.h"
#include "print.h"
#include "bool.h"

#include <iostream>
#include <fstream>

void read_gmsh(System *sys, Logger *logger);
void read_edges(System *sys, Logger *logger, vector<Node> terminal_nodes);
void add_edge_to_mesh(System *sys, int index, int num, int id1, int id2);
void create_map_matrix(System *sys);
void detect_terminal_edges(System *sys, Logger *logger, vector<Node> terminal_nodes);
vector<int> sort_vertex_ids(System *sys, int i);

#endif
