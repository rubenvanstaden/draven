#! /usr/bin/python

from networkx.drawing.nx_pydot import read_dot
import matplotlib.pyplot as plt 
import networkx as nx

G = read_dot('graph.dot')

print(nx.get_node_attributes(G,'pos'))

dot_pos = nx.get_node_attributes(G, 'pos')

pos = dict()
for key, value in dot_pos.items():
    postuple = tuple(map(float, value[1:-1].split(',')))
    pos[key] = postuple

print(pos)

nx.draw(G, pos=pos)

plt.show()
