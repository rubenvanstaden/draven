/*********************************************************************
 *
 *  Gmsh rectangle volume
 *
 *********************************************************************/

// Discretization size
lc1 = 5.0E-6;
lc2 = 5.0E-6;
lct = 5.0E-6;

// Strip
t1 = 220E-9;
w1 = 5E-6;
l1 = 50E-6;
layers1 = 2;
lambda1 = 137E-9;

// Ground
t2 = 300E-9;
a2 = 6E-6;
lambda2 = 86E-9;
layers2 = layers1;

h = 177.5E-9;

// Strip
Point(1) = {-l1/2.0, -w1/2.0, t2+h, lc1};
Point(2) = {-l1/2.0,  w1/2.0, t2+h, lc1};
Point(3) = { l1/2.0,  w1/2.0, t2+h, lc1};
Point(4) = { l1/2.0, -w1/2.0, t2+h, lc1};

// Ground
Point(5) = {-l1/2.0-a2, -w1/2.0-a2, 0, lc2};
Point(6) = {-l1/2.0-a2,  w1/2.0+a2, 0, lc2};
Point(7) = { l1/2.0+a2,  w1/2.0+a2, 0, lc2};
Point(8) = { l1/2.0+a2, -w1/2.0-a2, 0, lc2};

// Negative Terminal 1
Point(9)  = {      -l1/2.0, -w1/2.0, 0, lct};
Point(10) = {-l1/2.0-t1/10,     0.0, 0, lct};
Point(11) = {      -l1/2.0,  w1/2.0, 0, lct};
Point(12) = {-l1/2.0+t1/10,     0.0, 0, lct};

// Negative Terminal 2
Point(13) = {      l1/2.0, -w1/2.0, 0, lct};
Point(14) = {l1/2.0-t1/10,     0.0, 0, lct};
Point(15) = {      l1/2.0,  w1/2.0, 0, lct};
Point(16) = {l1/2.0+t1/10,     0.0, 0, lct};

// Strip
Line(21) = {1,2};
Line(22) = {2,3};
Line(23) = {3,4};
Line(24) = {4,1};

// Ground
Line(25) = {5,6};
Line(26) = {6,7};
Line(27) = {7,8};
Line(28) = {8,5};

// Negative Terminal 1
Line(29) = {9,10};
Line(30) = {10,11};
Line(31) = {11,12};
Line(32) = {12,9};

// Negative Terminal 2
Line(33) = {13,14};
Line(34) = {14,15};
Line(35) = {15,16};
Line(36) = {16,13};

// Strip
Line Loop(41) = {21,22,23,24};
// Ground
Line Loop(42) = {25,26,27,28};
// Negative Terminal 1
Line Loop(43) = {29,30,31,32};
// Negative Terminal 2
Line Loop(44) = {33,34,35,36};

// Strip
strip = 51;
Plane Surface(strip) = {41};
// Ground
Plane Surface(52) = {42,43,44};
// Negative Terminal 1
tn1 = 53;
Plane Surface(tn1) = {43};
// Negative Terminal 2
tn2 = 54;
Plane Surface(tn2) = {44};

// Strip
info1[] = Extrude {0,0,t1} {Surface{strip}; Layers{{layers1},{1}};};

// Ground
info2[] = Extrude {0,0,t2} {Surface{52}; Layers{{layers1},{1}};};

// Negative Terminal 1
info3[] = Extrude {0,0,t2} {Surface{tn1}; Layers{{layers1},{1}};};

// Negative Terminal 2
info4[] = Extrude {0,0,t2} {Surface{tn2}; Layers{{layers1},{1}};};


////////////////////////////////////////////////////////////////////////////////
// Properties
////////////////////////////////////////////////////////////////////////////////

// Frequencies (-s = start, -e = end, -n = steps):
Physical Point(".f -s 10E9 -e 10E9 -n 1") = {};

// Volume conductivity (-s) and penetration depth (-l):
Physical Volume(".v -s 0.0 -l 137E-9") = {info1[1]};
Physical Volume(".v -s 0.0 -l 86E-9") = {info2[1]};

Physical Point(".e 0 -p 0 -n 1") = {};
Physical Point(".e 1 -p 2 -n 3") = {};

// Excitation port 0 positive (-p) and negative (-n) terminals:
Physical Surface(".t1 0") = info1[2];	// side surface after extrusion
Physical Surface(".t2 1") = {info3[2],info3[3],info3[4],info3[5]};	// top surface after extrusion

// Excitation port 1 positive (-p) and negative (-n) terminals:
Physical Surface(".t3 2") = info1[4];	// side surface after extrusion
Physical Surface(".t4 3") = {info4[2],info4[3],info4[4],info4[5]};	// top surface after extrusion
