#include "vtk_write.h"

#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkRegularPolygonSource.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkGlyph2D.h>
#include <vtkCellArray.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleImage.h>

#include <vtkGlyph3D.h>
#include <vtkGlyphSource2D.h>
#include <vtkMutableDirectedGraph.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSimple2DLayoutStrategy.h>
#include <vtkArrowSource.h>

#include <vtkGraphLayout.h>
#include <vtkGraphLayoutView.h>
#include <vtkGraphToPolyData.h>

#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyData.h>
#include <vtkSphereSource.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>

#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkPolyLine.h>
#include <vtkTriangle.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkSmartPointer.h>

#include <vtkPoints.h>
#include <vtkLine.h>
#include <vtkCellArray.h>
#include <vtkSmartPointer.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>

void vertex_currents(System *sys, Logger *logger, cx_mat In)
{
    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            int id = sys->vecTri[i]->edge[j]->id - 1;
            sys->vecTri[i]->vertex[j]->curr_vector[0] += real(In(id));
            sys->vecTri[i]->vertex[j]->curr_vector[1] += imag(In(id));
            sys->vecTri[i]->vertex[j]->divid_num++;
        }
    }

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            if (sys->vecTri[i]->vertex[j]->divid_num > 0) {
                sys->vecTri[i]->vertex[j]->curr_vector[0] = sys->vecTri[i]->vertex[j]->curr_vector[0] / sys->vecTri[i]->vertex[j]->divid_num;
                sys->vecTri[i]->vertex[j]->curr_vector[1] = sys->vecTri[i]->vertex[j]->curr_vector[1] / sys->vecTri[i]->vertex[j]->divid_num;
                // Set back to zero, we have already found the average current at this vertex.
                sys->vecTri[i]->vertex[j]->divid_num = 0;
            }
        }
    }
}

void vtk_3d_graph(System *sys, Logger *logger)
{
    // Create 8 Vertices.
    vtkSmartPointer<vtkPoints> points =
    vtkSmartPointer<vtkPoints>::New();

    for ( unsigned int i = 0; i < 2; ++i )
        for ( unsigned int j = 0; j < 4; ++j )
            points->InsertNextPoint( i, j, 0);

    //Create Edges
    vtkSmartPointer<vtkPolyLine> line =
    vtkSmartPointer<vtkPolyLine>::New();
    line->GetPointIds()->SetNumberOfIds(8);

    for ( unsigned int i = 0; i < 8; ++i )
        line->GetPointIds()->SetId(i,i);

    vtkSmartPointer<vtkCellArray> cellArray =
    vtkSmartPointer<vtkCellArray>::New();
    cellArray->InsertNextCell(line);

    // Create a Graph with Vertices and Edges.
    vtkSmartPointer<vtkUnstructuredGrid> grid =
    vtkSmartPointer<vtkUnstructuredGrid>::New();
    grid->SetPoints(points);
    grid->SetCells(VTK_POLY_LINE, cellArray);

    // Write the file
    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer =
    vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
    writer->SetFileName("testVertex.vtu");
    writer->SetInputData(grid);
    writer->Write();
}

void vtk_internal_graph(System *sys, Logger *logger)
{
    // logger->printTitle("Writing VTK --> Internal Graphs");
    //
    // vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    // vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
    // vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    // vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    //
    // for (auto vert : sys->vecVert) {
    //     if (vert->type == TYPE_INTERNAL) {
    //         for (auto node : vert->g->vprop) {
    //             std::cout << node.second->id << std::endl;
    //
    //             double x = node.second->center.x;
    //             double y = node.second->center.y;
    //             double z = node.second->center.z;
    //
    //             points->InsertNextPoint(x, y, z);
    //         }
    //
    //         for (int i = 0; i < vert->g->V; i++) {
    //             for (auto edge : vert->g->adj[i]) {
    //                 int v = edge.first;
    //                 int w = edge.second;
    //
    //                 Triangle *tri1 = vert->g->vprop[i];
    //                 Triangle *tri2 = vert->g->vprop[v];
    //
    //                 std::cout << tri1->id << " : " << tri2->id << std::endl;
    //
    //                 // for (int i = 0; i < 2; i++) {
    //                 //     line->GetPointIds()->SetId(i, edge);
    //                 // }
    //             }
    //         }
    //
    //         // for (int i = 0; i < vert->g->V; i++) {
    //         //     list< pair<int, int> >::iterator j;
    //         //     for (j = vert->g->adj[i].begin(); j != vert->g->adj[i].end(); ++j) {
    //         //         int v = (*j).first;
    //         //         int weight = (*j).second;
    //         //
    //         //         Triangle *tri1 = vert->g->vprop[i];
    //         //         Triangle *tri2 = vert->g->vprop[v];
    //         //
    //         //         std::cout << tri1->id << " : " << tri2->id << std::endl;
    //         //     }
    //         //
    //             // for (int i = 0; i < 2; i++) {
    //             //     line->GetPointIds()->SetId(i, edge);
    //             // }
    //         // }
    //     }
    // }
    //
    // // vtkSmartPointer<vtkLine> line0 = vtkSmartPointer<vtkLine>::New();
    // // line0->GetPointIds()->SetId ( 0,0 );
    // // line0->GetPointIds()->SetId ( 1,1 );
    // //
    // // vtkSmartPointer<vtkLine> line1 = vtkSmartPointer<vtkLine>::New();
    // // line1->GetPointIds()->SetId ( 0,1 );
    // // line1->GetPointIds()->SetId ( 1,2 );
    // //
    // // vtkSmartPointer<vtkLine> line2 = vtkSmartPointer<vtkLine>::New();
    // // line2->GetPointIds()->SetId ( 0,2 );
    // // line2->GetPointIds()->SetId ( 1,0 );
    // //
    // // vtkSmartPointer<vtkCellArray> lines = vtkSmartPointer<vtkCellArray>::New();
    // // lines->InsertNextCell ( line0 );
    // // lines->InsertNextCell ( line1 );
    // // lines->InsertNextCell ( line2 );
    //
    // polydata->SetPoints(points);
    // // polydata->SetLines ( lines );
    //
    // writer->SetFileName("TriangleLines.vtp");
    // writer->SetInputData(polydata);
    // writer->Write();
}

void vtk_center_vectors(System *sys, Logger *logger)
{
    vtkSmartPointer<vtkDoubleArray> center_vectors = vtkSmartPointer<vtkDoubleArray>::New();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

    center_vectors->SetNumberOfComponents(3);
    center_vectors->SetName("center_vectors");

    double x, y, z;
    double vertex[3];

    for (int j = 0; j < sys->numTri; j++) {
        Triangle *tri = sys->vecTri[j];
        for (int i = 0; i < 3; i++) {
            Vertex *vert = tri->vertex[i];
            CenterVector *cen = tri->cenVect[i];

            if (cen->direct == DIRECT_NEG) {
                x = tri->center.x;
                y = tri->center.y;
                z = tri->center.z;

                vertex[0] = cen->vector.x;
                vertex[1] = cen->vector.y;
                vertex[2] = cen->vector.z;
            }
            else {
                x = vert->node->x;
                y = vert->node->y;
                z = vert->node->z;

                vertex[0] = cen->vector.x;
                vertex[1] = cen->vector.y;
                vertex[2] = cen->vector.z;
            }

            center_vectors->InsertNextTuple(vertex);
            points->InsertNextPoint(x, y, z);
        }
    }

    // for (auto tri : sys->vecTri) {
    //     for (int i = 0; i < 3; i++) {
    //         Vertex *vert = tri->vertex[i];
    //         CenterVector *cen = tri->cenVect[i];
    //
    //         double x = vert->node->x;
    //         double y = vert->node->y;
    //         double z = vert->node->z;
    //
    //         vertex[0] = cen->vector.x;
    //         vertex[1] = cen->vector.y;
    //         vertex[2] = cen->vector.z;
    //
    //         center_vectors->InsertNextTuple(vertex);
    //         points->InsertNextPoint(x, y, z);
    //     }
    // }

    vtkSmartPointer<vtkPolyData> pointsPolydata = vtkSmartPointer<vtkPolyData>::New();
    pointsPolydata->SetPoints(points);

    vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
    vertexFilter->SetInputData(pointsPolydata);
    vertexFilter->Update();

    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->DeepCopy(vertexFilter->GetOutput());
    polydata->GetPointData()->SetVectors(center_vectors);

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName("center_vect.vtp");
    writer->SetInputData(polydata);
    writer->Write();
}

void vtk_points(System *sys, Logger *logger)
{
    vtkSmartPointer<vtkDoubleArray> efield = vtkSmartPointer<vtkDoubleArray>::New();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();

    // Define some colors
    unsigned char red[3] = {0, 0, 0};
    unsigned char green[3] = {0, 255, 0};
    unsigned char blue[3] = {0, 0, 0};

    // Setup the colors array
    colors->SetNumberOfComponents(3);
    colors->SetName("Colors");

    for (auto vert : sys->vecVert) {
        double x = vert->node->x;
        double y = vert->node->y;
        double z = vert->node->z;

        if (vert->type == TYPE_INTERNAL)
            colors->InsertNextTypedTuple(red);
        else
            colors->InsertNextTypedTuple(green);

        points->InsertNextPoint(x,y,z);
    }

    vtkSmartPointer<vtkPolyData> pointsPolydata = vtkSmartPointer<vtkPolyData>::New();
    pointsPolydata->SetPoints(points);

    vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
    vertexFilter->SetInputData(pointsPolydata);
    vertexFilter->Update();

    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->DeepCopy(vertexFilter->GetOutput());
    polydata->GetPointData()->SetScalars(colors);

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName("points.vtp");
    writer->SetInputData(polydata);
    writer->Write();
}

// void vtk_points(System *sys, Logger *logger)
// {
//     logger->printTitle("Writing VTK --> Points");
//
//     vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
//     vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    // vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
//     vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
//
    // // Define some colors
    // unsigned char red[3] = {255, 0, 0};
    // unsigned char green[3] = {0, 255, 0};
    // unsigned char blue[3] = {0, 0, 255};
    //
    // // Setup the colors array
    // colors->SetNumberOfComponents(3);
    // colors->SetName("Colors");
//
//     // for (auto vert : sys->vecVert) {
//     //     double x = vert->node->x * 10e6;
//     //     double y = vert->node->y * 10e6;
//     //     double z = vert->node->z * 10e6;
//     //
//     //     std::cout << x << " " << y << "\n";
//     //
//     //     float vertex[3] = {0.0, 0.0, 0.0};
//     //
//     //     if (vert->type == TYPE_INTERNAL)
//     //         colors->InsertNextTupleValue(red);
//     //     else
//     //         colors->InsertNextTupleValue(green);
//     //
//     //     points->InsertNextPoint(x,y,z);
//     // }
//
//     points->InsertNextPoint(1, 0, 0);
//     points->InsertNextPoint(2, 0, 0);
//     points->InsertNextPoint(3, 0, 0);
//
//     polydata->SetPoints(points);
//     // polydata->GetPointData()->SetScalars(colors);
//
//     writer->SetFileName("points.vtp");
//     writer->SetInputData(polydata);
//     writer->Write();
// }

void vtk_triangle(System *sys, Logger *logger, cx_mat In)
{
    /**************************************************************
    * Unfortunately in this simple example the following lines are ambiguous.
    * The first 0 is the index of the triangle vertex which is ALWAYS 0-2.
    * The second 0 is the index into the point (geometry) array, so
    * this can range from 0-(NumPoints-1)
    * i.e. a more general statement is triangle->GetPointIds()->SetId(0, PointId);
    *
    * Notes
    * -----
    *    vertex_id <==> edge_id
    *
    *    Die vertex order in die triangle correspond met die
    *    edge order in die triangle, i.o.w. the edge[0] is the
    *    edge across vertex[0], and edge[1] is the edge across
    *    vertex[1], etc.
    **************************************************************/

    logger->printTitle("Writing VTK --> Triangles");

    vertex_currents(sys, logger, In);

    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkCellArray> triangles = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkTriangle> triangle = vtkSmartPointer<vtkTriangle>::New();
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkDoubleArray> colors = vtkSmartPointer<vtkDoubleArray>::New();
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();

    colors->SetNumberOfComponents(3);
    colors->SetName("Colors");

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            double x = sys->vecTri[i]->vertex[j]->node->x;
            double y = sys->vecTri[i]->vertex[j]->node->y;
            double z = sys->vecTri[i]->vertex[j]->node->z;

            double curr_x = float(sys->vecTri[i]->vertex[j]->curr_vector[0]);
            double curr_y = float(sys->vecTri[i]->vertex[j]->curr_vector[1]);
            double curr_z = 0;

            // double vertex[3] = {curr_x, curr_y, curr_z};
            double vertex[3] = {curr_x, curr_y, curr_z};

            points->InsertNextPoint(x,y,z);
            colors->InsertNextTuple(vertex);
        }

        cout << endl;
    }

    // Add the triangle to the list of triangles (in this case there is only 1)
    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++)
            triangle->GetPointIds()->SetId(j, 3*i + j);
        triangles->InsertNextCell(triangle);
    }

    // Add the geometry and topology to the polydata
    polydata->SetPoints(points);
    polydata->SetPolys(triangles);
    polydata->GetPointData()->SetVectors(colors);

    writer->SetFileName("current.vtp");
    writer->SetInputData(polydata);
    writer->Write();
}

void vtk_mesh(System *sys, Logger *logger)
{
    logger->printTitle("Writing VTK --> Mesh");

    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkCellArray> triangles = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkTriangle> triangle = vtkSmartPointer<vtkTriangle>::New();
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    vtkSmartPointer<vtkDoubleArray> colors = vtkSmartPointer<vtkDoubleArray>::New();

    colors->SetNumberOfComponents(3);
    colors->SetName("Colors");

    for (int i = 0; i < sys->numTri; i++) {
        bool boundary = false;
        bool terminal = false;

        // Vector from vertex to center node.
        bool cen_vect_pos = false;

        for (int j = 0; j < 3; j++) {
            if (sys->vecTri[i]->edge[j]->type == TYPE_BOUNDARY)
                boundary = true;
            if (sys->vecTri[i]->edge[j]->type == TYPE_TERMINAL)
                terminal = true;
            if (sys->vecTri[i]->edge[j]->cenVect[1] != NULL) {
                cen_vect_pos = true;
            }
        }

        for (int j = 0; j < 3; j++) {
            double x = sys->vecTri[i]->vertex[j]->node->x;
            double y = sys->vecTri[i]->vertex[j]->node->y;
            double z = sys->vecTri[i]->vertex[j]->node->z;

            double curr_x = 0.0;
            double curr_y = 0.0;
            double curr_z = 0.0;

            if (cen_vect_pos)
                curr_x = 0.5;
            else
                curr_x = 0.0;

            double vertex[3] = {curr_x, curr_y, curr_z};

            colors->InsertNextTuple(vertex);
            points->InsertNextPoint(x,y,z);
        }
    }

    // Add the triangle to the list of triangles (in this case there is only 1)
    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++)
            triangle->GetPointIds()->SetId(j, 3*i + j);
        triangles->InsertNextCell(triangle);
    }

    // Add the geometry and topology to the polydata
    polydata->SetPoints(points);
    polydata->SetPolys(triangles);
    polydata->GetPointData()->SetVectors(colors);

    writer->SetFileName("mesh.vtp");
    writer->SetInputData(polydata);
    writer->Write();
}

void vtk_edges(System *sys, Logger *logger)
{
    logger->printTitle("Writing VTK --> Edges");

    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkCellArray> edges = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkLine> edge = vtkSmartPointer<vtkLine>::New();
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    vtkSmartPointer<vtkDoubleArray> colors = vtkSmartPointer<vtkDoubleArray>::New();

    colors->SetNumberOfComponents(3);
    colors->SetName("Colors");

    for (int i = 0; i < sys->numEdge; i++) {
        for (int j = 0; j < 2; j++) {
            double x = sys->vecEdge[i]->vertex[j]->node->x;
            double y = sys->vecEdge[i]->vertex[j]->node->y;
            double z = sys->vecEdge[i]->vertex[j]->node->z;

            double curr_x = 0.0;
            double curr_y = 0.0;
            double curr_z = 0.0;

            if (sys->vecEdge[i]->type == TYPE_INTERNAL)
                if (sys->vecEdge[i]->cenVect[0]->direct == DIRECT_POS)
                    curr_x = 1.0;

            double vertex[3] = {curr_x, curr_y, curr_z};

            colors->InsertNextTuple(vertex);
            points->InsertNextPoint(x,y,z);
        }
    }

    // Add the triangle to the list of triangles (in this case there is only 1)
    for (int i = 0; i < sys->numEdge; i++) {
        for (int j = 0; j < 2; j++)
            edge->GetPointIds()->SetId(j, 2*i + j);
        edges->InsertNextCell(edge);
    }

    // Add the geometry and topology to the polydata
    polydata->SetPoints(points);
    polydata->SetLines(edges);
    polydata->GetPointData()->SetVectors(colors);

    writer->SetFileName("edges.vtp");
    writer->SetInputData(polydata);
    writer->Write();
}
