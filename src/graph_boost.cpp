#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/config.hpp>
#include <fstream>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/property_map/property_map.hpp>

#include "mesh.h"
#include "graph_boost.h"

// typedef boost::property<boost::edge_weight_t, int> EdgeWeightProperty;
// typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, VertexProperty, EdgeWeightProperty> iGraph;
//
// template <class WeightMap, class CapacityMap>
// class edge_writer {
//     public:
//         edge_writer(WeightMap w, CapacityMap c) : wm(w), cm(c) {}
//
//         template <class Vertex>
//         void operator()(ostream &out, const Vertex& e) const {
//             out << "[label=\"" << wm[e] << "\", pos=\""
//             << cm[e].first*10e9 << "," << cm[e].second*10e9 << "\"]";
//         }
//     private:
//         WeightMap wm;
//         CapacityMap cm;
// };
//
// template <class WeightMap, class CapacityMap>
// inline edge_writer<WeightMap, CapacityMap>
// make_edge_writer(WeightMap w, CapacityMap c) {
//     return edge_writer<WeightMap, CapacityMap>(w, c);
// }

void generate_graph(Vertex *vert, std::vector<VertexProperty> &graph_nodes)
{
    // iGraph g;
    //
    // std::cout << "Generating graph" << std::endl;
    // std::cout << "Graph size: " << graph_nodes.size() << std::endl;
    // std::cout << "Graph edges:" << std::endl;
    //
    // for (int i = 0; i < graph_nodes.size(); i++) {
    //     VertexProperty n1 = graph_nodes[i];
    //     int v1 = boost::add_vertex(n1, g);
    //     std::cout << "vertex name " << g[v1].Id << std::endl;
    // }
    //
    // std::cout << "Iterate over vertices:" << std::endl;
    // auto vs = boost::vertices(g);
    // for (auto vit = vs.first; vit != vs.second; ++vit)
    //     std::cout << "V: " << g[*vit].Id << std::endl;
    //
    // for (int i = 0; i < graph_nodes.size()-1; i++) {
    //     if (graph_nodes[i].Direction == DIRECT_NEG)
    //         boost::add_edge(i, i+1, g);
    //     else
    //         boost::add_edge(i+1, i, g);
    // }
    //
    // boost::add_edge(graph_nodes.size()-1, 0, g);
    //
    // std::cout << "Iterate over edges\n";
    // auto es = boost::edges(g);
    // for (auto eit = es.first; eit != es.second; ++eit)
    //     std::cout << g[boost::source(*eit, g)].Id << ' ' << g[boost::target(*eit, g)].Id << std::endl;
    //
    // ofstream dot("graph.dot");
    // boost::write_graphviz(dot, g,
    //     make_edge_writer(boost::get(&VertexProperty::Id, g),
    //                      boost::get(&VertexProperty::Pos, g)));
}
