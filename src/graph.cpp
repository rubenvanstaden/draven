#include "graph.h"
#include "logger.h"
#include "triangle.h"

// Create a priority queue to store vertices that
// are being preprocessed. This is weird syntax in C++.
// Refer below link for details of this syntax
// http://geeksquiz.com/implement-min-heap-using-stl/

// Create a vector for distances and initialize all
// distances as infinite (INF)

// Insert source itself in priority queue and initialize
// its distance as 0.

/* Looping till priority queue becomes empty (or all
  distances are not finalized) */

// The first vertex in pair is the minimum distance
// vertex, extract it from priority queue.
// vertex label is stored in second of pair (it
// has to be done this way to keep the vertices
// sorted distance (distance must be first item
// in pair)

// This is the first and last terminal
// edge the loop is going thought.

Graph::Graph(int v_len)
{
    V = v_len;
    adj = new list<iPair> [V];
}

Graph::~Graph() {}

void Graph::add_edge(int u, int v, int w)
{
    adj[u].push_back(std::make_pair(v, w));
    adj[v].push_back(std::make_pair(u, w));
}

// Function to print shortest path from source to 'j' using parent array
void print_path(int parent[], int j)
{
    // Base Case : If j is source
    if (parent[j] == -1)
        return;

    print_path(parent, parent[j]);
    printf("%d ", j);
}

// A utility function to print the constructed distance array
// http://www.geeksforgeeks.org/printing-paths-dijkstras-shortest-path-algorithm/
int print_solution(std::vector<int> dist, int V, int parent[])
{
    int src = 0;
    printf("Vertex\t  Distance\tPath");
    for (int i = 1; i < V; i++) {
        printf("\n%d -> %d \t\t %d\t\t%d ", src, i, dist[i], src);
        print_path(parent, i);
    }
}

void terminal_mesh(int parent[], int savedWeight[], int j, rowvec &vecTerm)
{
    if (savedWeight[j] == 0)
        return;

    terminal_mesh(parent, savedWeight, parent[j], vecTerm);
    vecTerm(j) = savedWeight[j];
}

void Graph::shortest_path(int src)
{
    priority_queue< iPair, std::vector <iPair> , std::greater<iPair> > pq;

    int parent[V];
    int savedWeight[V];

    for (int i = 0; i < V; i++) {
        parent[0] = -1;
        savedWeight[0] = 0;
    }

    std::vector<int> dist(V, INF);
    std::vector<int> path;

    pq.push(std::make_pair(0, src));
    dist[src] = 0;

    std::cout << "awe:" << std::endl;
    while (!pq.empty()) {
        int u = pq.top().second;
        pq.pop();

        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i) {
            int v = (*i).first;
            int weight = (*i).second;

            if (dist[v] > dist[u]) {
                parent[v] = u;
                path.push_back(v);
                std::cout << "qwee:" << std::endl;
                // std::cout << parent[v] << " : " << v << std::endl;
                savedWeight[v] = weight;
                dist[v] = dist[u] + 1;
                // std::cout << dist[v] << " : " << v << std::endl;
                pq.push(std::make_pair(dist[v], v));
            }
        }
    }

    printf("Print parent:\n");
    print_solution(dist, V, parent);

    std::cout << dist[2] << std::endl;

    rowvec vecTerm;
    vecTerm.zeros(V);

    std::cout << "awe:" << std::endl;
    for(auto p : path) {
        std::cout << p << std::endl;
    }

    std::cout << "Terminal edges:" << std::endl;
    // for (int i = 0; i < sys->numTri; i++) {
    //     if (sys->vecTri[i]->term != 0) {
    //         std::cout << sys->vecTri[i]->id << std::endl;
    //     }
    // }

    // for (auto tris : sys->vecTri) {
    //     if (tris->term != 0)
    //         std::cout << tris->id << std::endl;
    // }

    // vecTerm(2) = -1;
    // vecTerm(7) = 1;

    terminal_mesh(parent, savedWeight, V-1, vecTerm);

    std::cout.precision(0);
    std::cout.setf(ios::fixed);
    std::cout << right << setw(3);

    vecTerm.raw_print(std::cout, "terminal graph:");

    // for (int i = 0; i < sys->numEdge; i++)
    //     sys->M(0,i) = vecTerm(i);
}
