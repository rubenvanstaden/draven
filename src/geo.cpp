#include "geo.h"

void construct_geometry(System *sys, Logger *logger)
{
    // create_triangle_area(sys, logger);
    // create_triangle_center_nodes(sys, logger);
    // create_triangle_center_vectors(sys, logger);
    // connect_tri_to_edges(sys, logger);
    // connect_tri_to_vertex(sys, logger);
    // detect_boundary_vertex(sys, logger);
}

void create_triangle_area(System *sys, Logger *logger)
{
    logger->printTitle("Calculate triangle area");

    for (int i = 0; i < sys->numTri; i++) {
        Node vect_1 = vector_sub(sys->vecTri[i]->vertex[1]->node,
                                 sys->vecTri[i]->vertex[0]->node);
        Node vect_2 = vector_sub(sys->vecTri[i]->vertex[2]->node,
                                 sys->vecTri[i]->vertex[0]->node);
        Node vect_3 = vector_cross(&vect_1, &vect_2);
        sys->vecTri[i]->mArea = vector_norm(&vect_3) / 2;
    }
}

void create_triangle_center_nodes(System *sys, Logger *logger)
{
    if (sys->debug) {
        logger->printTitle("Create triangle center nodes");
        cout << fixed << setprecision(6);
    }

    detect_boundary_edges(sys, logger);

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            sys->vecTri[i]->center.x += sys->vecTri[i]->vertex[j]->node->x;
            sys->vecTri[i]->center.y += sys->vecTri[i]->vertex[j]->node->y;
            sys->vecTri[i]->center.z += sys->vecTri[i]->vertex[j]->node->z;
        }

        sys->vecTri[i]->center.x /= 3.0;
        sys->vecTri[i]->center.y /= 3.0;
        sys->vecTri[i]->center.z /= 3.0;

        if (sys->debug) {
            int id = sys->vecTri[i]->id;
            double x = sys->vecTri[i]->center.x/DIM;
            double y = sys->vecTri[i]->center.y/DIM;

            cout << "  - Tri " << id << ":" << "\t";
            cout << left
                 << setw(4) << x << "\t"
                 << setw(4) << y << "\n";
        }
    }
}

void create_triangle_center_vectors(System *sys, Logger *logger)
{
    logger->printTitle("Create triangle center vectors");

    // We have to loop through every triangle
    // because we need the triangle center node.
    for (auto tri : sys->vecTri) {
        int num = 0;
        for (auto edge : tri->edge) {
            if (tri->cenVect[num] == NULL) {
                CenterVector *cen = new CenterVector;

                cen->id = sys->numCent;

                if (edge->cenVect[0] == NULL) {
                    cen->direct = DIRECT_POS;
                    cen->vector = vector_sub(&tri->center, tri->vertex[num]->node);
                    edge->cenVect[0] = cen;
                } else {
                    cen->direct = DIRECT_NEG;
                    cen->vector = vector_sub(tri->vertex[num]->node, &tri->center);
                    edge->cenVect[1] = cen;
                }

                tri->cenVect[num] = cen;
                tri->cenVect[num]->tri = tri; // Redundant, change this.
                sys->numCent++;
            }

            num++;
        }
    }
}

// void create_triangle_center_vectors(System *sys, Logger *logger)
// {
//     logger->printTitle("Create triangle center vectors");
//
//     int cen_vert_id = 0;
//
//     for (int i = 0; i < sys->numTri; i++) {
//         CenterVector *cenVect[3];
//
//         int direct = get_center_direction(sys, i);
//
//         for (int j = 0; j < 3; j++) {
//             cenVect[j] = new CenterVector();
//             cenVect[j]->id = cen_vert_id;
//             cen_vert_id++;
//
//             int type = sys->vecTri[i]->edge[j]->type;
//
//             // All terminals and boundary edges will have a positive centervector.
//             if ((type == TYPE_TERMINAL) || (type == TYPE_BOUNDARY)) {
                // cenVect[j]->vector = vector_sub(&sys->vecTri[i]->center, sys->vecTri[i]->vertex[j]->node);
                // cenVect[j]->direct = DIRECT_POS;
//             }
//             else if (type == TYPE_INTERNAL) {
//                 // Does not already has a centervector saved.
//                 if (sys->vecTri[i]->edge[j]->cenVect[0] == NULL) {
//                     cenVect[j]->vector = vector_sub(sys->vecTri[i]->vertex[j]->node, &sys->vecTri[i]->center);
//                     cenVect[j]->direct = DIRECT_POS;
//                 } else {
                    // cenVect[j]->vector = vector_sub(sys->vecTri[i]->vertex[j]->node, &sys->vecTri[i]->center);
//                     cenVect[j]->direct = DIRECT_NEG;
//                 }
//             }
//
//             // Update the Triangle and Edge objects.
//             sys->vecTri[i]->cenVect[j] = cenVect[j];
//
//             if (sys->vecTri[i]->edge[j]->cenVect[0] == NULL)
//                 sys->vecTri[i]->edge[j]->cenVect[0] = cenVect[j];
//             else
//                 sys->vecTri[i]->edge[j]->cenVect[1] = cenVect[j];
//         }
//     }
//
//     // if (sys->debug)
//     //     print_center_vectors(sys, logger);
// }

void detect_boundary_edges(System *sys, Logger *logger)
{
    logger->printTitle("Detect boundary edges");

    for (int i = 0; i < sys->numVert; i++) {
        for (int j = 0; j < sys->numVert; j++) {
            int bVal = sys->boundaryMap(i,j);
            if (bVal != 0)
                if (sys->vecEdge[bVal-1]->type != TYPE_TERMINAL)
                    sys->vecEdge[bVal-1]->type = TYPE_BOUNDARY;
        }
    }

    // if (sys->debug == 1) {
    //     cout << "--> Boundary edges: ";
    //     for (int i = 0; i < sys->numEdge; i++)
    //         if (sys->vecEdge[i]->type == TYPE_BOUNDARY)
    //             cout << sys->vecEdge[i]->id << " ";
    //     cout << endl;
    // }

    // if (sys->debug)
    //     print_boundary_edges(sys);
}

void detect_boundary_vertex(System *sys, Logger *logger)
{
    logger->printTitle("Detect boundary nodes");

    for (auto vert : sys->vecVert) {
        for (auto id : vert->tri) {
            Triangle *tri = sys->vecTri[id-1];

            for (auto edge : tri->edge) {
                if (vert->id == edge->vertex[0]->id) {
                    if (edge->type == TYPE_BOUNDARY)
                        vert->type = TYPE_BOUNDARY;
                    else if (edge->type == TYPE_TERMINAL)
                        vert->type = TYPE_TERMINAL;
                    // else
                    //     vert->type = TYPE_INTERNAL;
                }
                if (vert->id == edge->vertex[1]->id) {
                    if (edge->type == TYPE_BOUNDARY)
                        vert->type = TYPE_BOUNDARY;
                    else if (edge->type == TYPE_TERMINAL)
                        vert->type = TYPE_TERMINAL;
                    // else
                    //     vert->type = TYPE_INTERNAL;
                }
            }
        }
    }
}

int get_center_direction(System *sys, int i)
{
    int direct = 0;

    for (int j = 0; j < 3; j++) {
        if ((sys->vecTri[i]->edge[j]->type != TYPE_BOUNDARY) && (sys->vecTri[i]->edge[j]->type != TYPE_TERMINAL)) {
            if (sys->vecTri[i]->edge[j]->cenVect[0])
                direct = sys->vecTri[i]->edge[j]->cenVect[0]->direct;
            else if (sys->vecTri[i]->edge[j]->cenVect[1])
                direct = sys->vecTri[i]->edge[j]->cenVect[1]->direct;
        }
        else {
            if (sys->vecTri[i]->edge[j]->cenVect[0])
                direct = sys->vecTri[i]->edge[j]->cenVect[0]->direct;
        }
    }

    if (direct == DIRECT_POS)
        direct = DIRECT_NEG;
    else if (direct == DIRECT_NEG)
        direct = DIRECT_POS;

    return direct;
}
