#include "induct.h"
#include "cons.h"
#include "vtk_write.h"

// This is the Z matrix
void impedance_matrix(System *sys, Logger *logger)
{
    logger->printTitle("Constructing z matrix");
    cout << fixed << setprecision(9);

    create_resistance_matrix(sys, logger);
    create_inductance_matrix(sys, logger);

    double freq_start = 10e9;
    sys->omega = 2 * PI * freq_start;

    //sys->Z = sys->R + (sys->L);
    sys->Z = sys->R + (sys->omega * sys->L);

    if (sys->debug) {
        cx_mat Zp(sys->Z);
        Zp.print("Z:");
    }
}

void create_mzmt_matrix(System *sys, Logger *logger)
{
    logger->printTitle("Constructing mzmt matrix");

    sp_cx_mat M = sys->M;
    sp_cx_mat Z = sys->Z;

    sp_cx_mat A = M*Z*M.t();

    mat real_mat = zeros<mat>(2,1);
    mat imag_mat = zeros<mat>(2,1);
    cx_mat Vs = zeros<cx_mat>(2,1);

    real_mat(0,0) = 1.0;
    real_mat(1,0) = 0.0;

    imag_mat(0,0) = 0.0;
    imag_mat(1,0) = 0.0;

    Vs.set_real(real_mat);
    Vs.set_imag(imag_mat);

    cx_mat b = Vs;

    cx_mat Im = spsolve(A, b);
    cx_mat In = M.t()*Im;

    vtk_triangle(sys, logger, In);

    if (sys->debug) {
        cx_mat Ap(A);
        Ap.print("A:");
        Im.print("Im:");
        In.print("In:");
    }
}

void create_resistance_matrix(System *sys, Logger *logger)
{
    Node tet_local_coord[3];
    double node_squareSum;
    double K_real;
    double K_imag;
    double sigma_real = 1.0e-9;
    double sigma_imag = 1.0e-9;
    double temp = 0;

    sp_mat real_mat = zeros<sp_mat>(8,8);
    sp_mat imag_mat = zeros<sp_mat>(8,8);
    sys->R = zeros<sp_cx_mat>(8,8);

    for (int i = 0; i < sys->numTri; i++) {
        K_real = sigma_real / (9 * sys->vecTri[i]->mArea);
        K_imag = sigma_imag / (9 * sys->vecTri[i]->mArea);

        node_squareSum = 0;
        for (int j = 0; j < 3; j++) {
            tet_local_coord[j] = vector_sub(sys->vecTri[i]->vertex[j]->node, &sys->vecTri[i]->center);
            node_squareSum += vector_dot(tet_local_coord[j], tet_local_coord[j]);
        }
        node_squareSum /= 20.0;

        for (int m = 0; m < 3; m++) {
            int id_m = sys->vecTri[i]->edge[m]->id - 1;
            CenterVector *cenVect_m = sys->vecTri[i]->cenVect[m];

            for (int n = 0; n < 3; n++) {
                int id_n = sys->vecTri[i]->edge[n]->id - 1;
                CenterVector *cenVect_n = sys->vecTri[i]->cenVect[n];

                if (id_n != id_m) {
                    temp = node_squareSum;
                    temp += vector_dot(cenVect_n->vector, cenVect_m->vector);
                    real_mat(id_m, id_n) += K_real*temp;
                    imag_mat(id_m, id_n) += K_imag*temp;
                }
            }
        }

        sys->R.set_real(real_mat);
        sys->R.set_imag(imag_mat);
    }

    if (sys->debug) {
        cx_mat Rp(sys->R);
        Rp.print("R:");
    }
}

void create_inductance_matrix(System *sys, Logger *logger)
{
    cout << endl;
    logger->printTitle("Calculate inductance matrix");

    sp_mat real_mat = zeros<sp_mat>(8,8);
    sp_mat imag_mat = zeros<sp_mat>(8,8);
    sys->L = zeros<sp_cx_mat>(8,8);

    for (int i = 0; i < sys->numTri; i++) {
        //progress_bar(i, sys->numTri-1, 100, 50);
        double z = 0;

        for (int j = 0; j < sys->numTri; j++) {
            double dot = 0;

            for (int m = 0; m < 3; m++) {
                if (sys->vecTri[i]->edge[m]->type == TYPE_INTERNAL) {
                    int id_1 = sys->vecTri[i]->edge[m]->id - 1;
                    int cen_dir_1 = sys->vecTri[i]->cenVect[m]->direct;
                    Node first_node = sys->vecTri[i]->cenVect[m]->vector;
                    //cout << "first_node --> " <<  first_node.x << " " << first_node.y << endl;
                    for (int n = 0; n < 3; n++) {
                        if (sys->vecTri[j]->edge[n]->type == TYPE_INTERNAL) {
                            int id_2 = sys->vecTri[j]->edge[n]->id - 1;
                            int cen_dir_2 = sys->vecTri[j]->cenVect[n]->direct;
                            Node second_node = sys->vecTri[j]->cenVect[n]->vector;
                            //printf("%E\n", second_node.x);
                            //cout << "second_node --> " <<  second_node.x << " " << second_node.y << endl;

                            double integral = calc_integral_m(sys->vecTri[i], sys->vecTri[j]);
                            dot = dot + integral*vector_dot(first_node, second_node);
                            dot = dot * 0.5 * (1.2566370614e-6/(4*M_PI));

                            if (id_1 == id_2)
                                imag_mat(id_1, id_2) += dot;
                            else if (cen_dir_1 == 1)
                                imag_mat(id_1, id_2) += dot;

                            //if ((id_1 == 1) && (id_2 == 1))
                                //if (cen_dir_1 == 1)
                                    //z = z + dot;
                        }
                    }
                }
            }

            sys->L.set_imag(imag_mat);
        }
    }

    if (sys->debug) {
        cx_mat Lp(sys->L);
        Lp.print("L:");
    }
}

double calc_integral_m(Triangle *tri_m, Triangle *tri_n)
{
    double sum_m = 0;

    for (int i = 0; i < quadrature_size_m; i++) {
        Node rm = calc_rm(tri_m, i);
        double sum_n = calc_integral_n(tri_n, rm);
        sum_m = sum_m + W_m[i]*sum_n;
    }

    return sum_m*2;
}

double calc_integral_n(Triangle *tri_n, Node rm)
{
    double sum_n = 0;

    for (int i = 0; i < quadrature_size_n; i++) {
        Node rn = calc_rn(tri_n, i);
        double f = calc_f(rm, rn);
        sum_n = sum_n + W_n[i]*f;
    }

    return sum_n;
}

Node calc_rm(Triangle *tri_m, int i)
{
    Node rm;

    rm.x = 0;
    rm.y = 0;
    rm.z = 0;

    double L3 = 1 - L1_m[i] - L2_m[i];

    rm = vector_mul_add(rm, L1_m[i], tri_m->vertex[0]->node);
    rm = vector_mul_add(rm, L2_m[i], tri_m->vertex[1]->node);
    rm = vector_mul_add(rm, L3, tri_m->vertex[2]->node);

    return rm;
}

Node calc_rn(Triangle *tri_n, int i)
{
    Node rn;

    rn.x = 0;
    rn.y = 0;
    rn.z = 0;

    double L3 = 1 - L1_n[i] - L2_n[i];

    rn = vector_mul_add(rn, L1_n[i], tri_n->vertex[0]->node);
    rn = vector_mul_add(rn, L2_n[i], tri_n->vertex[1]->node);
    rn = vector_mul_add(rn, L3, tri_n->vertex[2]->node);

    return rn;
}

double calc_f(Node rm, Node rn)
{
    double x_diff = rm.x - rn.x;
    double y_diff = rm.y - rn.y;
    double z_diff = rm.z - rn.z;

    double dist_rm_rn = sqrt(x_diff*x_diff + y_diff*y_diff + z_diff*z_diff);
    double f = 1.0 / dist_rm_rn;

    return f;
}
