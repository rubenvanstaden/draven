#include "bool.h"

bool has_node_been_visited(vector<int> &visited_nodes, int id)
{
    bool is_visited = false;

    for (int i = 0; i < visited_nodes.size(); i++)
        if (visited_nodes[i] == id)
            is_visited = true;

    return is_visited;
}

bool is_triangle_connected_to_node(Triangle *edge_triangle, Vertex *node)
{
    bool is_connected = false;
    for (int i = 0; i < 3; i++)
        if (edge_triangle->vertex[i]->id == node->id)
            is_connected = true;
    return is_connected;
}

bool is_terminal_triangle(Triangle *tri)
{
    bool term = false;

    for (int i = 0; i < 3; i++)
        if (tri->edge[i]->type == TYPE_TERMINAL)
            term = true;

    return term;
}

bool is_edge_list_zero(int numEdge)
{
    bool edge_list_zero = true;

    if (numEdge > 0)
        edge_list_zero = false;

    return edge_list_zero;
}

bool does_edge_exist(vector<Edge *> edge, int numEdge, int id1, int id2)
{
    bool exists = false;

    for (int i = 0; i < numEdge; i++) {
        int edge_id_1 = edge[i]->vertex[0]->id;
        int edge_id_2 = edge[i]->vertex[1]->id;

        if ((edge_id_1 == id1) || (edge_id_1 == id2))
            if ((edge_id_2 == id2) || (edge_id_2 == id1))
                exists = true;
    }

    return exists;
}

bool is_center_negative(int direct)
{
    bool isNeg = false;
    if (direct == DIRECT_NEG)
        isNeg = true;
    return isNeg;
}

bool is_new_edge_equal_to_old_edge(Edge *edge_old, Edge *edge_new)
{
    bool is_not_equal = false;
    if (edge_new->id != edge_old->id)
        is_not_equal = true;
    return is_not_equal;
}

bool is_triangle_reached(vector<Triangle *> &tri_added, Triangle *tri)
{
    bool is_in_vector = false;

    if (tri_added.size() > 1)
        for (int i = 0; i < tri_added.size() - 1; i++)
            if (tri_added[0]->id == tri_added[i+1]->id)
                is_in_vector = true;

    return is_in_vector;
}

bool is_current_triangle_pos(Edge *edge, Triangle *tri)
{
    bool is_positive = false;
    if (edge->tri[0]->id == tri->id)
        is_positive = true;
    return is_positive;
}

bool is_current_triangle_neg(Edge *edge, Triangle *tri)
{
    bool is_negative = false;
    if (edge->tri[1]->id == tri->id)
        is_negative = true;
    return is_negative;
}
