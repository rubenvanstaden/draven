#include "print.h"

void loadBar(int x, int n, int r, int w)
{
    if (x % (n / r + 1) != 0) return;

    float ratio = x / (float) n;
    int c = ratio * w;

    printf("%3d%% [", (int) (ratio * 100));

    for (int x = 0; x < c; x++)
        printf("%c[1;36m=\033[0m", 27);
    for (int x = c; x < w; x++)
        printf(" ");

    if (ratio != 1)
        printf("]\n\033[F\033[J");
    else
        printf("]\n");
}

void progress_bar(int x, int n, int r, int w)
{
    float ratio;
    int c, i;

    // Only update r times.
    if (x % (n / r+1) != 0)
        return;
    // Calculuate the ratio of complete-to-incomplete.
    ratio = x / (float)n;
    c = (int)(ratio * w);

    // Show the percentage complete.
    printf("%3d%% [", (int)(ratio * 100));

    // Show the load bar.
    for (i = 0; i < c; i++)
        printf("%c[1;36m=\033[0m", 27);

    for (i = c; i < w; i++)
        printf(" ");

    printf("]");

    // ANSI Control codes to go back to the
    // previous line and clear it.
    if (x != n)
        printf("\r"); // Move to the first column
    else
        printf("\n");
    fflush(stdout);
}

void print_boundary_vertex(System *sys)
{
    for (int i = 0; i < sys->numVert; i++)
        cout << "boundary: " << sys->vecVert[i]->id << " : " << sys->vecVert[i]->type << endl;
}

void print_terminal_edges(System *sys, Logger *logger)
{
    logger->printTitle("Terminal edges");

    for (int i = 0; i < sys->numEdge; i++)
        if (sys->vecEdge[i]->type == TYPE_TERMINAL)
            cout << sys->vecEdge[i]->id << " ";
}

void print_edges(System *sys, Logger *logger)
{
    logger->printTitle("Edge ids");
    cout << "Format: edge_id --> node_1 node_2" << endl;

    for (int i = 0; i < sys->numEdge; i++) {
        cout << sys->vecEdge[i]->id;
        cout << " --> ";
        cout << sys->vecEdge[i]->vertex[0]->id;
        cout << " ";
        cout << sys->vecEdge[i]->vertex[1]->id;
        cout << endl;
    }
}

void print_tri_edges(System *sys, Logger *logger)
{
    logger->printTitle("Triangle edges");
    cout << "Format: tri_id --> edge_1 edge_2 edge_3" << endl;

    for (int i = 0; i < sys->numTri; i++) {
        int e1 = sys->vecTri[i]->edge[0]->id;
        int e2 = sys->vecTri[i]->edge[1]->id;
        int e3 = sys->vecTri[i]->edge[2]->id;

        cout << e1 << " " << e2 << " " << e3 << endl;
    }
}

void print_boundary_edges(System *sys)
{
    cout << "Boundary edges:" << endl;

    for (int i = 0; i < sys->numEdge; i++) {
        if (sys->vecEdge[i]->type == TYPE_BOUNDARY)
            printf("%d\n", sys->vecEdge[i]->id);
    }
}

void print_p_vector_edges(System *sys)
{
    cout << "P vector edges:" << endl;

    for (int i = 0; i < sys->numEdge; i++) {
        if (sys->vecEdge[i]->type != TYPE_TERMINAL) {
            if (sys->vecEdge[i]->type != TYPE_BOUNDARY) {
                Node center1 = sys->vecEdge[i]->cenVect[0]->vector;
                Node center2 = sys->vecEdge[i]->cenVect[1]->vector;

                printf("%d: %E %E %E\n", sys->vecEdge[i]->id, center1.x,
                                         center1.y, center1.z);
                printf("%d: %E %E %E\n", sys->vecEdge[i]->id, center2.x,
                                         center2.y, center2.z);
            }
        }
    }
}

void print_vertex(System *sys, Logger *logger)
{
    cout.precision(1);
    logger->printTitle("Node ids");
    cout << "Format: node_id --> x y z (um)" << endl;

    for (int i = 0; i < sys->numVert; i++) {
        cout << sys->vecVert[i]->id;
        cout << " --> ";
        cout << sys->vecVert[i]->node->x/DIM;
        cout << " ";
        cout << sys->vecVert[i]->node->y/DIM;
        cout << " ";
        cout << sys->vecVert[i]->node->z/DIM;
        cout << endl;
    }
    cout << fixed;
}

void print_edge_triangles(System *sys, Logger *logger)
{
    logger->printTitle("Edge triangles");
    cout << "Format: edge_id --> tri_pos tri_neg" << endl;

    for (int i = 0; i < sys->numEdge; i++) {
        if (sys->vecEdge[i]->type == TYPE_INTERNAL) {
            cout << sys->vecEdge[i]->id;
            cout << " ";
            cout << sys->vecEdge[i]->tri[0]->id;
            cout << " ";
            cout << sys->vecEdge[i]->tri[1]->id;
            cout << endl;
        }
    }
}

void print_triangles(System *sys, Logger *logger)
{
    logger->printTitle("Triangle ids");
    cout << "Format: tri_id --> vertex_1 vertex_2 vertex_3" << endl;

    for (int i = 0; i < sys->numTri; i++) {
        cout << sys->vecTri[i]->id;
        cout << " --> ";
        cout << sys->vecTri[i]->vertex[0]->id;
        cout << " ";
        cout << sys->vecTri[i]->vertex[1]->id;
        cout << " ";
        cout << sys->vecTri[i]->vertex[2]->id;
        cout << endl;
    }
}

void print_center_vectors(System *sys, Logger *logger)
{
    logger->printTitle("Triangle center vectors");

    for (int i = 0; i < sys->numTri; i++) {
        cout << "--> Triangle id: " << sys->vecTri[i]->id << endl;

        for (int j = 0; j < 3; j++) {
            int id = sys->vecTri[i]->cenVect[j]->id;

            double x = sys->vecTri[i]->cenVect[j]->vector.x/DIM;
            double y = sys->vecTri[i]->cenVect[j]->vector.y/DIM;

            cout << "  - Edge " << id << ":" << "\t";
            cout << left
                 << setw(4) << x << "\t"
                 << setw(4) << y << "\n";
        }
    }
}
