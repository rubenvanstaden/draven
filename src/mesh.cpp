#include "json.hpp"
#include "mesh.h"
#include <iostream>
#include <fstream>

#include <boost/graph/subgraph.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/config.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/property_map/property_map.hpp>

#include "graph_boost.h"

typedef boost::property<boost::edge_weight_t, int> EdgeWeightProperty;
// typedef boost::subgraph<boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, VertexProperty, EdgeWeightProperty>> iGraph;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, VertexProperty, EdgeWeightProperty> iGraph;


/**************************************************************
 * Hacker: 1h3d*n
 * For: Volundr
 * Docs: Algorithm 5
 * Date: 19 April 2017
 *
 * Description: Calculating the internal mesh matrix, M.
 *
 * 1) A vector is created in the Class Mesh that contains
 *    all the internal nodes of the structure. These nodes
 *    form the vertices of the triangles that meshes the
 *    structure.
 *
 * 2) Loop throught each node that is not a boundary or
 *    terminal node and get the first triangle
 *    that shares a vertex with this node. That means the
 *    triangle is connected to the node. This is called
 *    the main triangle. And since it is the first traingle
 *    we will also save this as the orgin triangle, so we
 *    can know when we have moved 360 degrees around the node.
 *
 * 3) Loop throught the edges of this triangle and look at
 *    each edge's two connected triangles.
 *
 * 4) Move to the triangle of the edge that's ID is not
 *    equal to the main triangle's. Call this the edge triangle.
 *
 * 5) Make sure this edge triangle is also connected to the node.
 *
 * 6) The main triangle will now be the first vertex of
 *    the graph and the edge triangle will be the second.
 *
 * 7) The edge in the Class Graph that connects, main
 *    triangle and edge triangle, will be a 1/-1 depending
 *    on whether the main triangle is the edge's positive
 *    or negative triangle.
 *
 * 8) Once the two vertices and the edge is saved in the
 *    graph object, we can move the main triangle to be
 *    the edge's triangle. Then repeat this process to add
 *    to the graph object, untill we get the first triangle
 *    again.
 *
 * Creating terminal mesh row vectors:
 *
 * 1) Connect graph node of current triangle to other graph
 *    nodes connected to this triangle.
 *
 * 2) Look at the triangle that is not the current one we
 *    are looking at.
 *
 * 3) The first triangle in the edge is the positive one.
 *    If n==1 it means the main triangle is the positive
 *    one and that we are moving from the positive to the
 *    negative triangle connected to the edge.
 **************************************************************/

using json = nlohmann::json;

void internal_mesh(System *sys, Logger *logger);
void terminal_mesh(System *sys, Logger *logger);
void create_terminal_mesh(System *sys, Logger *logger);

std::vector<json> json_triangle(System *sys, Logger *logger)
{
    std::vector<json> Triangles {};

    for (int i = 0; i < sys->numTri; i++) {
        json Object;

        std::vector<double> center_vector {sys->vecTri[i]->center.x,
                                           sys->vecTri[i]->center.y,
                                           sys->vecTri[i]->center.z};
        json center_vec(center_vector);

        std::vector<int> vertex_vector {sys->vecTri[i]->vertex[0]->id,
                                        sys->vecTri[i]->vertex[1]->id,
                                        sys->vecTri[i]->vertex[2]->id};
        json vertex_vec(vertex_vector);

        std::vector<int> cen_vector {sys->vecTri[i]->cenVect[0]->id,
                                     sys->vecTri[i]->cenVect[1]->id,
                                     sys->vecTri[i]->cenVect[2]->id};
        json cen_vec(cen_vector);

        std::vector<int> edge_vector {sys->vecTri[i]->edge[0]->id,
                                      sys->vecTri[i]->edge[1]->id,
                                      sys->vecTri[i]->edge[2]->id};
        json edge_vec(edge_vector);

        Object["id"] = sys->vecTri[i]->id;
        Object["term"] = sys->vecTri[i]->term;
        Object["center"] = center_vec;
        Object["vertex"] = vertex_vec;
        Object["cen_vect"] = cen_vec;
        Object["edge"] = edge_vec;

        Triangles.push_back(Object);
    }

    return Triangles;
}

std::vector<json> json_edge(System *sys, Logger *logger)
{
    std::vector<json> Edges {};

    for (int i = 0; i < sys->numEdge; i++) {
        json Object;

        std::vector<int> vertex_vector {sys->vecEdge[i]->vertex[0]->id, sys->vecEdge[i]->vertex[1]->id};
        json vertex_vec(vertex_vector);

        std::vector<int> cen_vector;
        std::vector<int> tri_vector;

        if (sys->vecEdge[i]->type == TYPE_INTERNAL) {
            cen_vector.push_back(sys->vecEdge[i]->cenVect[0]->id);
            cen_vector.push_back(sys->vecEdge[i]->cenVect[1]->id);

            tri_vector.push_back(sys->vecEdge[i]->tri[0]->id);
            tri_vector.push_back(sys->vecEdge[i]->tri[1]->id);
        }
        else {
            cen_vector.push_back(sys->vecEdge[i]->cenVect[0]->id);
            tri_vector.push_back(sys->vecEdge[i]->tri[0]->id);
        }

        json cen_vec(cen_vector);
        json tri_vec(tri_vector);

        Object["id"] = sys->vecEdge[i]->id;
        Object["type"] = sys->vecEdge[i]->type;
        Object["vertex"] = vertex_vec;
        Object["cen_vect"] = cen_vec;
        Object["tri"] = tri_vec;

        Edges.push_back(Object);
    }

    return Edges;
}

std::vector<json> json_vertex(System *sys, Logger *logger)
{
    std::vector<json> Vertex {};

    for (int i = 0; i < sys->numVert; i++) {
        json Object;

        std::vector<double> node_vector {sys->vecVert[i]->node->x, sys->vecVert[i]->node->y, sys->vecVert[i]->node->z};
        json node_vec(node_vector);
        json tri_vec(sys->vecVert[i]->tri);

        Object["id"] = sys->vecVert[i]->id;
        Object["type"] = sys->vecVert[i]->type;
        Object["node"] = node_vec;
        Object["tri"] = tri_vec;

        Vertex.push_back(Object);
    }

    return Vertex;
}

std::vector<json> json_center(System *sys, Logger *logger)
{
    std::vector<json> Center {};

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            json Object;

            Object["id"] = sys->vecTri[i]->cenVect[j]->id;
            Object["direct"] = sys->vecTri[i]->cenVect[j]->direct;
            Object["tri"] = sys->vecTri[i]->id;

            Center.push_back(Object);
        }
    }

    return Center;
}

// This is the M matrix
void mesh_matrix(System *sys, Logger *logger)
{
    // TODO: Implement to read the terminal label.
    for (int i = 0; i < sys->numTri; i++)
        if (is_terminal_triangle(sys->vecTri[i]))
            sys->vecTri[i]->term = 1;

    json jsonMain;

    std::vector<json> Triangles = json_triangle(sys, logger);
    std::vector<json> Edges = json_edge(sys, logger);
    std::vector<json> Vertex = json_vertex(sys, logger);
    std::vector<json> Center = json_center(sys, logger);

    jsonMain["Triangles"] = Triangles;
    jsonMain["Edges"] = Edges;
    jsonMain["Vertex"] = Vertex;
    jsonMain["Center"] = Center;

    std::ofstream json_file("mesh.json");
    json_file << std::setw(4) << jsonMain << std::endl;
<<<<<<< HEAD

    // detect_boundary_vertex(sys, logger);
    internal_mesh(sys, logger);
    terminal_mesh(sys, logger);

=======
    
    // detect_boundary_vertex(sys);
    // create_internal_mesh(sys, logger);
    // create_terminal_mesh(sys, logger);
    
>>>>>>> c7d5295fc819445c87984b7d50e3ddccd665f182
    // if (sys->debug) {
    //     cout << fixed << setprecision(0);
    //     cout << right << setw(3);
    //
    //     cx_mat Mp(sys->M);
    //     Mp.raw_print(cout, "M:");
    // }
}

/**************************************************************
 * Description:
 * ------------
 * Recursively loop over all triangles that are
 * connected to the internal node. We have to do a recursive
 * since we don't know the amount of triangles conneted.
 **************************************************************/

bool is_back_at_source_triangle(const int tri_id, const int src_id)
{
    bool visited = false;
    if (tri_id == src_id)
        visited = true;
    return visited;
}

bool is_current_triangle(const int tri_id, const int cur_id)
{
    bool visited = false;
    if (tri_id == cur_id)
        visited = true;
    return visited;
}

bool is_already_visited(const int tri_id, std::vector<int> &graph_nodes)
{
    bool visited = false;
    for (auto id : graph_nodes) {
        if (id == tri_id)
            visited = true;
    }
    return visited;
}

int recursive_node_loop(std::map<int, int> node_map, std::vector<int> &graph_nodes,
    iGraph &g, auto src, auto curr, int node_id, int v1, int v2)
{
    // Loop every edge of the connected triangle,
    // that is also connected to the interal node.
    for (auto edge : curr->edge) {
        if ((edge->vertex[0]->id == node_id) || (edge->vertex[1]->id == node_id)) {
            // Look at the two triangles connected
            // to the edge and save the one that is
            // not equal to the current triangle.
            for (int j = 0; j < 2; j++) {
                bool visited = false;

                auto tri_next = edge->tri[j];

                visited = is_current_triangle(edge->tri[j]->id, curr->id);
                visited = is_back_at_source_triangle(edge->tri[j]->id, src->id);
                visited = is_already_visited(edge->tri[j]->id, graph_nodes);

                if (!visited) {
                    // // The first triangle is negative
                    // if (j == 0)
                    //     vProp.Direction = DIRECT_NEG;
                    // else
                    //     vProp.Direction = DIRECT_POS;

                    v2 = node_map[tri_next->id];
                    std::cout << tri_next->id << " " << v2 << std::endl;

                    boost::add_edge(v1, v2, g);

                    v1 = v2;

                    graph_nodes.push_back(tri_next->id);

                    v2 = recursive_node_loop(node_map, graph_nodes, g, src, tri_next, node_id, v1, v2);
                }
            }
        }
    }

    return v2;
}

void convert_map_to_graph(Vertex *vert, std::map<int, myPair> &nodeloop)
{
    // int len = nodeloop.size() - 1;
    // int num = nodeloop.size() + 1;

    // Graph *graph = new Graph(num);
    //
    // std::cout << "Graph edges:" << std::endl;
    // for (int i = 0; i < len; i++) {
    //     std::cout << nodeloop[i].first->id << " => " << nodeloop[i+1].first->id << std::endl;
    //
    //     Triangle *tri1 = nodeloop[i].first;
    //     int w = nodeloop[i+1].second;
    //
    //     graph->vprop.emplace(i, tri1);
    //     graph->add_edge(i, i+1, w);
    // }
    //
    // std::cout << nodeloop[len].first->id << " => " << nodeloop[0].first->id << std::endl;
    //
    // // Add the last edge connecting the
    // // end and begin triangles of the circle.
    // Triangle *tri1 = nodeloop[len].first;
    // int w = nodeloop[len].second;
    //
    // // graph->vprop.emplace(len, tri1);
    // // graph->add_edge(len, 0, w);
    //
    // std::cout << "Graph vertex properties:" << std::endl;
    //
    // for (auto v : graph->vprop) {
    //     std::cout << v.first << " : " << v.second->id << std::endl;
    // }
    //
    // vert->g = graph;
    // graph->shortest_path(0);
}

template <class WeightMap, class CapacityMap>
class edge_writer {
    public:
        edge_writer(WeightMap w, CapacityMap c) : wm(w), cm(c) {}

        template <class Vertex>
        void operator()(ostream &out, const Vertex& e) const {
            out << "[label=\"" << wm[e] << "\", pos=\""
            << cm[e].first*10e9 << "," << cm[e].second*10e9 << "\"]";
        }
    private:
        WeightMap wm;
        CapacityMap cm;
};

template <class WeightMap, class CapacityMap>
inline edge_writer<WeightMap, CapacityMap>
make_edge_writer(WeightMap w, CapacityMap c) {
    return edge_writer<WeightMap, CapacityMap>(w, c);
}

void internal_mesh(System *sys, Logger *logger)
{
    logger->printTitle("Create internal mesh graph");

    int internal = 0;
    int boundary = 0;
    int terminal = 0;

    for (auto vert : sys->vecVert) {
        if (vert->type == TYPE_INTERNAL)
            internal++;
        if (vert->type == TYPE_BOUNDARY)
            boundary++;
        if (vert->type == TYPE_TERMINAL)
            terminal++;
    }

    std::cout << "Internal: " << internal << std::endl;
    std::cout << "Boundary: " << boundary << std::endl;
    std::cout << "Terminal: " << terminal << std::endl;

    iGraph g;

    std::map<int, int> node_map; // Triangle -- Graph Node link

    for (auto vert : sys->vecVert) {
        if (vert->type == TYPE_INTERNAL) {
            for (int i = 0; i < vert->tri.size(); i++) {
                VertexProperty vProp;

                auto tri = sys->vecTri[i];

                vProp.Id = tri->id;
                vProp.Tri = tri;
                vProp.Source = true;
                vProp.Direction = DIRECT_EMPTY;

                vProp.Pos.first = vProp.Tri->center.x;
                vProp.Pos.second = vProp.Tri->center.y;

                int v = boost::add_vertex(vProp, g);
                node_map[tri->id] = v;
            }
        }
    }

    std::cout << "\nPrint node_map:" << std::endl;
    for (auto const& [key, value] : node_map)
        std::cout << key << " : " << value << std::endl;
    std::cout << "\n";

    for (auto vert : sys->vecVert) {
        if (vert->type == TYPE_INTERNAL) {
            std::vector<int> graph_nodes;

            const int id = vert->tri[0];
            auto tri = sys->vecTri[id];

            int v1 = node_map[tri->id];
            int v2 = 10;

            graph_nodes.push_back(tri->id);

            v2 = recursive_node_loop(node_map, graph_nodes, g, tri, tri, vert->id, v1, v2);

            std::cout << v1 << " " << v2 << std::endl;

            boost::add_edge(v1, v2, g);

            ofstream dot("graph.dot");
            boost::write_graphviz(dot, g,
                make_edge_writer(boost::get(&VertexProperty::Id, g),
                                 boost::get(&VertexProperty::Pos, g)));

            // generate_graph(vert, graph_nodes);
            // directed_generate_graph(vert, graph_nodes);

            // convert_map_to_graph(vert, nodeloop);

            // vert->internal_graph = nodeloop;
        }
    }
}

void terminal_mesh(System *sys, Logger *logger)
{

}

// void terminal_mesh(System *sys, Logger *logger)
// {
//     logger->printTitle("Create terminal mesh graph");
//
//     Graph *cGraph = new Graph(sys->numTri);
//     // cGraph->V = sys->numTri;
//
//     for (int i = 0; i < sys->numTri; i++) {
//         if (is_terminal_triangle(sys->vecTri[i]))
//             cGraph->type = TYPE_TERMINAL;
//
//         for (int j = 0; j < 3; j++){
//             Edge *edge = sys->vecTri[i]->edge[j];
//
//             if (edge->type == TYPE_INTERNAL) {
//                 for (int n = 0; n < 2; n++) {
//                     if (edge->tri[n] != NULL) {
//                         if (sys->vecTri[i]->id != edge->tri[n]->id) {
//                             connect_to_graph(sys, logger, cGraph, edge, n, sys->vecTri[i]->id);
//                             edge->graph_connect = true;
//                         }
//                     }
//                 }
//             }
//         }
//     }
//
//     cGraph->shortest_path(0);
// }

// void create_internal_mesh(System *sys, Logger *logger)
// {
//     logger->printTitle("Create internal mesh graph");
//
//     sys->M.resize(2,8);
//
//     for (int i = 0; i < sys->numVert; i++) {
//         if (sys->vecVert[i]->type == TYPE_INTERNAL) {
//             Vertex *vertex = sys->vecVert[i];
//             int id = sys->vecVert[i]->tri[0];
//             Triangle *tri = sys->vecTri[id];
//
//             vector<Triangle *> tri_added;
//
//             Edge *edge = new Edge();
//             edge = NULL;
//
//             for (int n = 0; n < 3; n++) {
//                 edge = tri->edge[n];
//                 edge = get_new_edge_connected_to_vertex(tri->edge[n], tri);
//
//                 if (edge != NULL)
//                     break;
//             }
//
//             int cols = 0;
//             rowvec row;
//             row.zeros(8);
//
//             if (edge != NULL)
//                 jump_over_edge_to_triangle(sys, tri_added, tri, edge, vertex, cols, row);
//
//             if (sys->debug) {
//                 cout << "Tri id --> ";
//                 for (int i = 0; i < (int)tri_added.size(); i++)
//                     cout << tri_added[i]->id << " ";
//                 cout << endl;
//             }
//
//             for (int j = 0; j < 8; j++) {
//                 sys->M(1,j) = row(j);
//             }
//         }
//     }
// }

<<<<<<< HEAD
=======
// void create_terminal_mesh(System *sys, Logger *logger)
// {
//     logger->printTitle("Create terminal mesh graph");
// 
//     Graph *cGraph = new Graph;
//     cGraph->V = sys->numTri;
// 
//     for (int i = 0; i < sys->numTri; i++) {
//         if (is_terminal_triangle(sys->vecTri[i]))
//             cGraph->type = TYPE_TERMINAL;
// 
//         for (int j = 0; j < 3; j++){
//             Edge *edge = sys->vecTri[i]->edge[j];
// 
//             if (edge->type == TYPE_INTERNAL) {
//                 for (int n = 0; n < 2; n++) {
//                     if (edge->tri[n] != NULL) {
//                         if (sys->vecTri[i]->id != edge->tri[n]->id) {
//                             connect_to_graph(sys, logger, cGraph, edge, n, sys->vecTri[i]->id);
//                             edge->graph_connect = true;
//                         }
//                     }
//                 }
//             }
//         }
//     }
// 
//     cGraph->shortest_path(sys, 0);
// }

>>>>>>> c7d5295fc819445c87984b7d50e3ddccd665f182
// void jump_over_edge_to_triangle(System *sys, vector<Triangle *> &tri_added, Triangle *tri, Edge *edge, Vertex *vertex, int cols, rowvec &row)
// {
//     if (!is_triangle_reached(tri_added, tri)) {
//         tri_added.push_back(tri);
//
//         if (is_current_triangle_pos(edge, tri)) {
//             row(edge->id-1) = 1;
//             tri = move_to_next_traingle(edge->tri[1]);
//             edge = get_new_edge_connected_to_vertex(edge, tri);
//             jump_over_edge_to_triangle(sys, tri_added, tri, edge, vertex, cols, row);
//         }
//         if (is_current_triangle_neg(edge, tri)) {
//             row(edge->id-1) = -1;
//             tri = move_to_next_traingle(edge->tri[0]);
//             edge = get_new_edge_connected_to_vertex(edge, tri);
//             jump_over_edge_to_triangle(sys, tri_added, tri, edge, vertex, cols, row);
//         }
//     }
// }
//
// Triangle *move_to_next_traingle(Triangle *tri)
// {
//     Triangle *tri_new = new Triangle();
//     tri_new = tri;
//     return tri_new;
// }
//
// Edge *get_new_edge_connected_to_vertex(Edge *edge, Triangle *tri)
// {
//     Edge *edge_new = new Edge();
//     edge_new = NULL;
//
//     for (int i = 0; i < 3; i++) {
//         if (tri->edge[i]->type == TYPE_INTERNAL) {
//             if (is_new_edge_equal_to_old_edge(edge, tri->edge[i])) {
//                 edge_new = tri->edge[i];
//                 break;
//             }
//         }
//     }
//
//     return edge_new;
// }
