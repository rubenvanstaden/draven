#include "tools.h"
#include <boost/filesystem.hpp>
#include "boost/progress.hpp"
#include <iostream>
#include <stdexcept>

namespace fs = boost::filesystem;

void file_system(System *sys, std::string test_name, Logger *logger) {
    logger->printTitle("Reading file_system...");

    boost::filesystem::path project_root_path( boost::filesystem::current_path() );
    std::cout << "Project root path is : " << project_root_path << std::endl;

    std::string test_dir(project_root_path.string() + "/tests/");
    std::string test_run(test_dir + test_name + "/");
    fs::path test_path(test_run);
    cout << "Test path: " << test_path << endl;

    unsigned long file_count = 0;
    unsigned long dir_count = 0;
    unsigned long other_count = 0;
    unsigned long err_count = 0;

    if (!fs::exists(test_path)) {
        printf("\n%c[1;31mERROR:\033[0m ", 27);
        printf("Example file specified does not exist\n");
        printf("Use ");
        printf("%c[1;36m-s\033[0m", 27);
        printf(" or ");
        printf("%c[1;36m--samples\033[0m", 27);
        printf(" to view usable examples\n\n");

        exit(0);
    }

    sys->m_files["dir"] = test_run;

    if (fs::is_directory(test_path)) {
        std::cout << "Files found:" << endl;
        fs::directory_iterator end_iter;

        for (fs::directory_iterator dir_itr(test_path); dir_itr != end_iter; ++dir_itr) {
            try {
                if (fs::is_directory(dir_itr->status())) {
                    ++dir_count;
                    std::cout << dir_itr->path().filename() << " [directory]\n";
                }
                else if (fs::is_regular_file(dir_itr->status())) {
                    ++file_count;
                    string filename = dir_itr->path().filename().string();
                    string extension = fs::extension(filename);
                    sys->m_files[extension] = filename;
                }
                else {
                    ++other_count;
                    std::cout << dir_itr->path().filename() << " [other]\n";
                }
            }
            catch (const std::exception &ex) {
                ++err_count;
                std::cout << dir_itr->path().filename() << " " << ex.what() << std::endl;
            }
        }
    }
    else
        std::cout << "\nFound: " << test_path << "\n";

    // Make sure that all needed files
    // are in the directory.
    bool geo = false;
    bool msh = false;

    for (const auto& pair : sys->m_files) {
        if (pair.first == ".geo")
            geo = true;
        if (pair.first == ".msh")
            msh = true;

        std::cout << "-- " << pair.first << ": " << pair.second << "\n";
    }

    if (!geo)
        throw std::invalid_argument(".geo file missing");
    if (!msh)
        throw std::invalid_argument(".msh file missing");
}

void print_info(System *sys)
{
    cout << endl;
    cout << endl;
    cout << "Running TriHenry:" << endl;
    cout << "   -- Version: " << "\033[1;32m" << VERSION << "\033[0m" << endl;
    cout << "   -- Date: " << DATE << endl;
    cout << "   -- Hacker: " << "\033[1;36m" << HACKER << "\033[0m" << endl;
    cout << "   -- Developer: " << DEVELOPERS << endl;
    cout << endl;
}

void set_options(System *sys, Logger *logger)
{
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        logger->printTitle("Working directory");
        cout << "\033[1;31m" << cwd << "\033[0m" << endl;
    } else
        perror("getcwd() error");
}

void write_time(int day)
{
    ofstream myfile;
    myfile.open ("day");
    myfile << day << endl;
    myfile.close();
}

void read_quote(int num)
{
    int count = 0;

    string line;
    string author = "";
    string quote = "";

    ifstream myfile ("quote");
    if (myfile.is_open()) {
        while(getline(myfile, line)) {
            if (line.empty())
                count++;
            else if (line[0] == '-') {
                if (count == num)
                    author = line;
            }
            else {
                if (count == num)
                    quote += line + "\n";
            }
        }
        myfile.close();
    }
    else
        throw std::invalid_argument("Unable to open Quote file.");

    // Remove last new line char from quote.
    if (!quote.empty() && quote[quote.length()-1] == '\n')
        quote.erase(quote.length()-1);

    cout << '"' << "\033[1;33m" << quote << "\033[0m" << '"' << endl;
    cout << author;
}

void create_quote()
{
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    int saved_day = 0;

    srand((unsigned)time(0));
    int num_quote = rand() % 30;

    // AUtomate this part
    // ================================
    chdir("../");
    boost::filesystem::path project_root_path(boost::filesystem::current_path());
    // std::cout << "Project root path is : " << project_root_path << std::endl;

    // std::string test_dir(project_root_path.string() + "/day");
    // std::string test_run(test_dir + test_name + "/");
    // fs::path test_path(test_run);
    // cout << "Test path: " << test_path << endl;
    // ================================

    string line;
    ifstream myfile (project_root_path.string() + "/day");
    if (myfile.is_open()) {
        while ( getline (myfile,line) ) {
            string str(line);
            string buf;
            stringstream ss(str);

            vector<string> tokens;

            while (ss >> buf)
                tokens.push_back(buf);

            saved_day = atoi(tokens[0].c_str());
        }
        myfile.close();
    }
    else
        throw std::invalid_argument("Unable to open Day file.");

    read_quote(num_quote);
}
