#include "connect.h"

<<<<<<< HEAD
void connect_to_graph(System *sys, Logger *logger, Graph *cGraph, Edge *edge, int n, int id)
{
    for (int i = 0; i < sys->numTri; i++) {
        if (sys->vecTri[i]->id == edge->tri[n]->id) {
            if (!edge->graph_connect) {
                if (n == 0)
                    cGraph->add_edge(id-1, edge->tri[n]->id-1, -1);
                else
                    cGraph->add_edge(id-1, edge->tri[n]->id-1, 1);
            }
        }
    }
}
=======
// void connect_to_graph(System *sys, Logger *logger, Graph *cGraph, Edge *edge, int n, int id)
// {
//     logger->printTitle("connect_to_graph");
// 
//     for (int i = 0; i < sys->numTri; i++) {
//         if (sys->vecTri[i]->id == edge->tri[n]->id) {
//             if (!edge->graph_connect) {
//                 if (n == 0)
//                     cGraph->addEdge(id-1, edge->tri[n]->id-1, -1);
//                 else
//                     cGraph->addEdge(id-1, edge->tri[n]->id-1, 1);
//             }
//         }
//     }
// }
>>>>>>> c7d5295fc819445c87984b7d50e3ddccd665f182

void connect_tri_to_edges(System *sys, Logger *logger)
{
    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            if (sys->vecTri[i]->edge[j]->type == TYPE_INTERNAL) {
                if (sys->vecTri[i]->cenVect[j]->direct == DIRECT_POS)
                    sys->vecTri[i]->edge[j]->tri[0] = sys->vecTri[i];
                if (sys->vecTri[i]->cenVect[j]->direct == DIRECT_NEG)
                    sys->vecTri[i]->edge[j]->tri[1] = sys->vecTri[i];
            }
            else {
                sys->vecTri[i]->edge[j]->tri[0] = sys->vecTri[i];
            }
        }
    }

    // for (int i = 0; i < sys->numEdge; i++) {
    //     if (!sys->vecEdge[i]->tri[0])
    //         cout << "e: " << i << endl;
    // }

    if (sys->debug) {
        print_tri_edges(sys, logger);
        print_edge_triangles(sys, logger);
    }
}

void connect_edge_to_tri(System *sys, Logger *logger, int index, int num, int id1, int id2)
{
    for (int i = 0; i < sys->numEdge; i++) {
        int edge_id_1 = sys->vecEdge[i]->vertex[0]->id;
        int edge_id_2 = sys->vecEdge[i]->vertex[1]->id;

        // cout << sys->vecEdge[i]->vertex[0]->node->x*10e15 << " " << sys->vecEdge[i]->vertex[0]->node->y << endl;

        if ((edge_id_1 == id1) || (edge_id_1 == id2)) {
            if ((edge_id_2 == id2) || (edge_id_2 == id1)) {
                sys->vecTri[index]->edge[num] = sys->vecEdge[i];

                if (sys->debug)
                    cout << num << " : " << index << " : " << sys->vecEdge[i]->id << endl;
            }
        }
    }
}

void connect_center_to_tri_and_edge(Triangle *tri, Logger *logger, int i, int j, CenterVector *cenVect)
{
    tri->cenVect[j] = cenVect;

    if (cenVect->direct != DIRECT_EMPTY) {
        if (is_center_negative(cenVect->direct))
            tri->edge[j]->cenVect[1] = cenVect;
        else
            tri->edge[j]->cenVect[0] = cenVect;
    } else {
        cout << "'ERROR: Why is center vector zero?" << endl;
    }
}

void connect_tri_to_vertex(System *sys, Logger *logger)
{
    for (int j = 0; j < sys->numVert; j++) {
        vector<int> tri_vertex;

        for (int i = 0; i < sys->numTri; i++) {
            for (int k = 0; k < 3; k++) {
                if (sys->vecTri[i]->vertex[k]->id == sys->vecVert[j]->id) {
                    tri_vertex.push_back(sys->vecTri[i]->id);
                }
            }
        }

        sys->vecVert[j]->tri = tri_vertex;

        // for (int i = 0; i < tri_vertex.size(); i++)
        //     sys->vecVert[j]->tri.push_back(tri_vertex[i]);
    }
}
