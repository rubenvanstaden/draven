#include "read.h"
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <string>
#include <iostream>
#include <regex>
#include <tuple>

using boost::tokenizer;
using boost::escaped_list_separator;

std::tuple<std::string, int, int, int> split_line_physicalnames(System *sys, string line);
void split_line_vertex(System *sys, string line);
void split_line_triangle(System *sys, string line, vector<int> &visited_nodes);
void split_line_terminals(System *sys, string line, vector<Node> &terminal_nodes);

void read_gmsh(System *sys, Logger *logger)
{
    /**************************************************************
     * http://gmsh.info/doc/texinfo/gmsh.html#File-formats
     *
     * $MeshFormat
     * 2.2 0 8
     * $EndMeshFormat
     * $Nodes
     * 6                      six mesh nodes:
     * 1 0.0 0.0 0.0          node #1: coordinates (0.0, 0.0, 0.0)
     * 2 1.0 0.0 0.0          node #2: coordinates (1.0, 0.0, 0.0)
     * 3 1.0 1.0 0.0          etc.
     * 4 0.0 1.0 0.0
     * 5 2.0 0.0 0.0
     * 6 2.0 1.0 0.0
     * $EndNodes
     * $Elements
     * 2                      two elements:
     * 1 3 2 99 2 1 2 3 4     quad #1: type 3, physical 99, elementary 2, nodes 1 2 3 4
     * 2 3 2 99 2 2 5 6 3     quad #2: type 3, physical 99, elementary 2, nodes 2 5 6 3
     * $EndElements
     * $NodeData
     * 1                      one string tag:
     * "A scalar view"        the name of the view ("A scalar view")
     * 1                      one real tag:
     * 0.0                    the time value (0.0)
     * 3                      three integer tags:
     * 0                      the time step (0; time steps always start at 0)
     * 1                      1-component (scalar) field
     * 6                      six associated nodal values
     * 1 0.0                  value associated with node #1 (0.0)
     * 2 0.1                  value associated with node #2 (0.1)
     * 3 0.2                  etc.
     * 4 0.0
     * 5 0.2
     * 6 0.4
     * $EndNodeData
     **************************************************************/

    logger->printTitle("Read nodes");

    std::string line;
    std::string msh_file = sys->m_files["dir"] + sys->m_files[".msh"];
    cout << "Mesh file read: " << msh_file << endl;
    ifstream myfile(msh_file);

    int count = 0;
    sys->numVert = 0;
    sys->numTri = 0;
    sys->numEdge = 0;

    bool readVertex = false;
    bool readTriangle = false;
    bool readTerminals = false;
    bool readPhysicalNames = false;

    vector<int> visited_nodes;
    vector<Node> terminal_nodes;

    std::vector<std::tuple<std::string, int, int, int>> terminals;

    if (myfile.is_open()) {
        while (getline(myfile, line)) {
            stringstream ss(line);
            string word;

            std::string EndNodes("$EndPhysicalNames");
            std::string Line(line);

            if (line.find("$EndPhysicalNames") != string::npos)
                readPhysicalNames = false;
            if (line.find("$EndNodes") != string::npos)
                readVertex = false;
            if (line.find("$EndElements") != string::npos) {
                readTriangle = false;
                readTerminals = false;
            }

            if (readPhysicalNames) {
                auto term = split_line_physicalnames(sys, line);
                // if (tuple_size<decltype(term)>::value > 0)
                if (get<0>(term).find(".t") != string::npos)
                    terminals.push_back(term);
            }
            if (readVertex)
                split_line_vertex(sys, line);
            if (readTriangle)
                split_line_triangle(sys, line, visited_nodes);
            if (readTerminals)
                split_line_terminals(sys, line, terminal_nodes);

            for (int wordNum = 1; ss >> word; wordNum++) {
                if (word.compare("$PhysicalNames") == 0) {
                    getline(myfile, line);
                    readPhysicalNames = true;
                }
                if (word.compare("$Nodes") == 0) {
                    getline(myfile, line);
                    sys->numVert = atoi(line.c_str());
                    readVertex = true;
                }
                if (word.compare("$Elements") == 0) {
                    getline(myfile, line);
                    readTriangle = true;
                    readTerminals = true;
                }
            }
        }

        myfile.close();
    } else
        throw std::invalid_argument("Unable to open MSH file.");

    for (auto t : terminals)
        cout << get<0>(t) << endl;

    read_edges(sys, logger, terminal_nodes);

    if (sys->debug) {
        print_vertex(sys, logger);
        print_triangles(sys, logger);
    }

    cout << "Number of Triangles: " << sys->numTri << endl;
    cout << "Number of Edges: " << sys->numEdge << endl;
}

// The -1 is to accoutn for the indexing in colMap/initmap
// change this asap mother fcukers.
void read_edges(System *sys, Logger *logger, vector<Node> terminal_nodes)
{
    logger->printTitle("Read edges");

    for (int i = 0; i < sys->numTri; i++) {
        vector<int> vectVertID = sort_vertex_ids(sys, i);

        int id_1 = vectVertID[0];
        int id_2 = vectVertID[1];
        int id_3 = vectVertID[2];

        add_edge_to_mesh(sys, i, 0, id_2, id_3);
        add_edge_to_mesh(sys, i, 1, id_1, id_3);
        add_edge_to_mesh(sys, i, 2, id_1, id_2);

        connect_edge_to_tri(sys, logger, i, 0, id_2, id_3);
        connect_edge_to_tri(sys, logger, i, 1, id_1, id_3);
        connect_edge_to_tri(sys, logger, i, 2, id_1, id_2);
    }

    create_map_matrix(sys);
    detect_terminal_edges(sys, logger, terminal_nodes);

    if (sys->debug)
        print_edges(sys, logger);
}

typedef tokenizer<escaped_list_separator<char> > so_tokenizer;

std::tuple<std::string, int, int, int> split_line_physicalnames(System *sys, string line)
{
    vector<std::string> strs;
    // boost::split(strs, line, boost::is_any_of(" "));

    std::regex rgx("\"([^\"]*)\"");
    auto words_begin = std::sregex_iterator(line.begin(), line.end(), rgx);
    auto words_end = std::sregex_iterator();

    for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
        std::smatch match = *i;
        std::string match_str = match.str();
        strs.push_back(match_str);
    }

    int type = line[0] - '0';
    int id = line[2] - '0';

    std::string row = strs[0].substr(1, strs[0].size()-2);

    vector<std::string> physical;
    boost::split(physical, row, boost::is_any_of(" "));

    std::string term_tag(physical[0].begin(), physical[0].end());
    std::string term_num(physical[1].begin(), physical[1].end());

    tuple <std::string, int, int, int> emerge;
    emerge = std::make_tuple(term_tag, type, id, atoi(term_num.c_str()));

    return emerge;
}

void split_line_vertex(System *sys, string line)
{
    string str(line);
    string buf;
    stringstream ss(str);

    vector<string> tokens;

    while (ss >> buf)
        tokens.push_back(buf);

    Vertex *vert = new Vertex();
    vert->id = atoi(tokens[0].c_str());
    vert->node->x = atof(tokens[1].c_str());
    vert->node->y = atof(tokens[2].c_str());
    vert->node->z = atof(tokens[3].c_str());

    sys->vecVert.push_back(vert);
}

void split_line_triangle(System *sys, string line, vector<int> &visited_nodes)
{
    string str(line);
    string buf;
    stringstream ss(str);

    vector<string> tokens;

    while (ss >> buf)
        tokens.push_back(buf);

    if (atoi(tokens[1].c_str()) == 2) {
        Triangle *tri = new Triangle();
        tri->id = sys->numTri + 1;

        int vertex_id_1 = atoi(tokens[5].c_str()) - 1;
        int vertex_id_2 = atoi(tokens[6].c_str()) - 1;
        int vertex_id_3 = atoi(tokens[7].c_str()) - 1;

        tri->vertex[0] = sys->vecVert[vertex_id_1];
        tri->vertex[1] = sys->vecVert[vertex_id_2];
        tri->vertex[2] = sys->vecVert[vertex_id_3];

        sys->vecTri.push_back(tri);

        // Connect triangles to vertex objects
        if (!has_node_been_visited(visited_nodes, vertex_id_1)) {
            sys->vecVert[vertex_id_1]->tri.push_back(sys->vecTri[sys->numTri]->id);
            visited_nodes.push_back(vertex_id_1);
        }
        if (!has_node_been_visited(visited_nodes, vertex_id_2)) {
            sys->vecVert[vertex_id_2]->tri.push_back(sys->vecTri[sys->numTri]->id);
            visited_nodes.push_back(vertex_id_2);
        }
        if (!has_node_been_visited(visited_nodes, vertex_id_3)) {
            sys->vecVert[vertex_id_3]->tri.push_back(sys->vecTri[sys->numTri]->id);
            visited_nodes.push_back(vertex_id_3);
        }

        sys->numTri++;
    }
}

void split_line_terminals(System *sys, string line, vector<Node> &terminal_nodes)
{
    string str(line);
    string buf;
    stringstream ss(str);

    vector<string> tokens;

    while (ss >> buf)
        tokens.push_back(buf);

    // Read the nodes that are connected
    // to the different terminals.
    // Save the two edge vertices of
    // the terminal in the x and y
    // members of a node object.
    if (atoi(tokens[1].c_str()) == 1) {
        Node node;
        node.id = atoi(tokens[0].c_str());
        node.x = atof(tokens[5].c_str());
        node.y = atof(tokens[6].c_str());

        cout << "Term Nodes: " << node.x << " " << node.y << endl;

        terminal_nodes.push_back(node);
    }
}

void detect_terminal_edges(System *sys, Logger *logger, vector<Node> terminal_nodes)
{
    for (int i = 0; i < sys->numEdge; i++) {
        int edge_node_1 = sys->vecEdge[i]->vertex[0]->id;
        int edge_node_2 = sys->vecEdge[i]->vertex[1]->id;

        for (int j = 0; j < terminal_nodes.size(); j++) {
            // if ((terminal_nodes[j].x == edge_node_1) || (terminal_nodes[j].x == edge_node_2))
            //     if ((terminal_nodes[j].y == edge_node_1) || (terminal_nodes[j].y == edge_node_2))
            //         sys->vecEdge[i]->type = TYPE_TERMINAL;

            if ((terminal_nodes[j].x == edge_node_1) && (terminal_nodes[j].y == edge_node_2))
                sys->vecEdge[i]->type = TYPE_TERMINAL;
            if ((terminal_nodes[j].x == edge_node_2) && (terminal_nodes[j].y == edge_node_1))
                sys->vecEdge[i]->type = TYPE_TERMINAL;
        }
    }

    if (sys->debug)
        print_terminal_edges(sys, logger);
}

void add_edge_to_mesh(System *sys, int index, int num, int id1, int id2)
{
    Edge *cEdge = new Edge();

    bool edge_list_zero = is_edge_list_zero(sys->numEdge);
    bool is_edge_in_list = does_edge_exist(sys->vecEdge, sys->numEdge, id1, id2);

    if (!is_edge_in_list || edge_list_zero) {
        Vertex *v1 = new Vertex();
        Vertex *v2 = new Vertex();

        for (int i = 0; i < sys->numVert; i++) {
            if (sys->vecVert[i]->id == id1)
                v1 = sys->vecVert[i];
            if (sys->vecVert[i]->id == id2)
                v2 = sys->vecVert[i];
        }

        cEdge->create_edge(sys->numEdge, v1, v2);
        sys->vecEdge.push_back(cEdge);
        sys->numEdge++;
    }
}

void create_map_matrix(System *sys)
{
    sys->initMap.zeros(sys->numVert, sys->numVert);
    sys->conMap.zeros(sys->numVert, sys->numVert);
    sys->boundaryMap.zeros(sys->numVert, sys->numVert);
    sys->edgeIdMap.zeros(sys->numVert, sys->numVert);

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            int v1 = sys->vecTri[i]->edge[j]->vertex[0]->id - 1;
            int v2 = sys->vecTri[i]->edge[j]->vertex[1]->id - 1;

            if (sys->initMap(v1, v2) == 0)
                sys->initMap(v1, v2) = sys->vecTri[i]->id;
            else
                sys->conMap(v1, v2) = sys->vecTri[i]->id;

            if (sys->initMap(v2, v1) == 0)
                sys->initMap(v2, v1) = sys->vecTri[i]->id;
            else
                sys->conMap(v2, v1) = sys->vecTri[i]->id;
        }
    }

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            int v1 = sys->vecTri[i]->edge[j]->vertex[0]->id - 1;
            int v2 = sys->vecTri[i]->edge[j]->vertex[1]->id - 1;

            if ((sys->initMap(v1,v2) != 0) && (sys->conMap(v1,v2) == 0))
                sys->boundaryMap(v1,v2) = sys->vecTri[i]->edge[j]->id;
            if ((sys->initMap(v2,v1) != 0) && (sys->conMap(v2,v1) == 0))
                sys->boundaryMap(v2,v1) = sys->vecTri[i]->edge[j]->id;
            if ((sys->initMap(v1,v2) != 0) && (sys->conMap(v1,v2) != 0))
                sys->edgeIdMap(v1,v2) = sys->vecTri[i]->edge[j]->id;
            if ((sys->initMap(v2,v1) != 0) && (sys->conMap(v2,v1) != 0))
                sys->edgeIdMap(v2,v1) = sys->vecTri[i]->edge[j]->id;
        }
    }

    // if (sys->debug) {
    //     cout.precision(0);
    //     cout.setf(ios::fixed);
    //     cout << right << setw(3);
    //
    //     sys->initMap.raw_print(cout, "ini_map:");
    //     cout << endl;
    //     sys->conMap.raw_print(cout, "con_map:");
    //     cout << endl;
    //     sys->boundaryMap.raw_print(cout, "boundary_map:");
    //     cout << endl;
    //     sys->edgeIdMap.raw_print(cout, "edge_id_map:");
    // }
}

// Rearange the triangle vertices in rising order
vector<int> sort_vertex_ids(System *sys, int i)
{
    vector<int> vectVertID(3);

    vectVertID[0] = sys->vecTri[i]->vertex[0]->id;
    vectVertID[1] = sys->vecTri[i]->vertex[1]->id;
    vectVertID[2] = sys->vecTri[i]->vertex[2]->id;

    Vertex *temp[3];

    sort(vectVertID.begin(), vectVertID.end());

    for (int j = 0; j < 3; j++) {
        if (sys->vecTri[i]->vertex[j]->id == vectVertID[0])
            temp[0] = sys->vecTri[i]->vertex[j];
        if (sys->vecTri[i]->vertex[j]->id == vectVertID[1])
            temp[1] = sys->vecTri[i]->vertex[j];
        if (sys->vecTri[i]->vertex[j]->id == vectVertID[2])
            temp[2] = sys->vecTri[i]->vertex[j];
    }

    sys->vecTri[i]->vertex[0] = temp[0];
    sys->vecTri[i]->vertex[1] = temp[1];
    sys->vecTri[i]->vertex[2] = temp[2];

    vectVertID[0] = sys->vecTri[i]->vertex[0]->id;
    vectVertID[1] = sys->vecTri[i]->vertex[1]->id;
    vectVertID[2] = sys->vecTri[i]->vertex[2]->id;

    return vectVertID;
}







