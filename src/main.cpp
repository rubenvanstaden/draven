#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <time.h>

#include "tools.h"
#include "read.h"
#include "mesh.h"
#include "induct.h"
#include "print.h"
#include "logger.h"
#include "args.h"
#include "types.h"
#include "graph_boost.h"
#include "vtk_write.h"
#include <boost/version.hpp>

/**************************************************************
 * Hacker: 1h3d*n
 * For: Volundr
 * Docs: Main
 * Date: 19 April 2017
 *
 * Installations
 * -------------
 *     VTK --> http://kazenotaiyo.blogspot.co.za/2010/06/installing-vtk-in-ubuntu-and-making.html
 *
 * Termnology
 * ----------
 *     --> Nodes are all the lose triangle vertices in the structure.
 *     --> Edge is the edge connecting two nodes.
 *     --> Trianlge is the polygon constructed by three edges.
 *     --> Vertex is the nodes connected to that triangle.
 *
 * Notes
 * -----
 *     1) The first center vector in the edge is the positive vector
 *        and the second is the negative.
 *     2) The first triangle connected to the edge is the edge's
 *        positive triangle.
 *
 * GDB
 * ---
 *     (gdb) set confirm off
 *     (gdb) backtrace
 **************************************************************/

void start_program(System *sys, Logger *logger);

// using boost::math::constants::pi;

#include <map>
#include <unistd.h>
#include <docopt/docopt.h>

typedef std::map<char, Node> BasePairMap;

bool to_bool(std::string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    std::istringstream is(str);
    bool b;
    is >> std::boolalpha >> b;
    return b;
}

static const char *const USAGE =
    R"(Usage:
        draven <test-name> [-v | --verbose | -d | --debug]

        Options:
            <test-name>    Test folder name
            -v --verbose   Verbose mode
            -d --debug     Debugging mode
            --version
)";

char *getcwd(char *buf, size_t size);

int main(int argc, char *argv[])
{
    System *sys = new System;
    Logger *logger = new Logger;

    sys->debug = false;
    logger->verbose = true;

    auto args = docopt::docopt(USAGE, {argv+1, argv+argc}, true, "David-Blaine 1.0.0");
    for(auto const& arg : args)
        std::cout << arg.first << ": " <<  arg.second << std::endl;

    if (args["--verbose"].asBool())
        logger->verbose = true;
    if (args["--debug"].asBool())
        sys->debug = true;

    start_program(sys, logger);

    if (args["<test-name>"].isString()) {
        file_system(sys, args["<test-name>"].asString(), logger);
        read_gmsh(sys, logger);

        // ======================================================================
        // Constructing Geometry
        // ======================================================================
        create_triangle_area(sys, logger);
        create_triangle_center_nodes(sys, logger);
        create_triangle_center_vectors(sys, logger);

        connect_tri_to_edges(sys, logger);
        connect_tri_to_vertex(sys, logger);
        detect_boundary_vertex(sys, logger);

        cout << "Number of Nodes: " << sys->numVert << endl;

        // ======================================================================
        // Plotting to VTK
        // ======================================================================
        // vtk_3d_graph(sys, logger);
        vtk_center_vectors(sys, logger);
        vtk_points(sys, logger);
        vtk_edges(sys, logger);
        vtk_mesh(sys, logger);

        // ======================================================================
        // Current and Impedance Calculations
        // ======================================================================
        mesh_matrix(sys, logger);
        // impedance_matrix(sys, logger);
        // create_mzmt_matrix(sys, logger);

        // vtk_internal_graph(sys, logger);
    }
    // =========================================================================

    return 0;
}

void start_program(System *sys, Logger *logger)
{
    print_info(sys);
    create_quote();
    set_options(sys, logger);
    cout << "Boost version: " << BOOST_LIB_VERSION;
}
