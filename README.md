# tetrahenry

# Developement

- Header file herarchy:
    * Main -> Read.h
           -> Mesh.h
           -> Inductance.h
           -> Print.h

    * Edge.h -> Triangle.h -> CenterVector.h -> Vertex.h -> Structures.h
    * Connect.h -> Bool.h -> Edge.h
                -> Graph.h
    * Read.h -> Connect.h
    * Mesh.h -> Geometry.h -> Connect.h
                           -> Vector.h -> Edge.h
                           -> Print.h
    * Inductance.h -> Vector.h
    * Print.h -> Edge.h

## Notes

* Should be keep the cenVect *id* equal to the Edge id? Or should we make it unique?
* Is it possible to make all the *center_vectors* in each triangle in the same direction (all in, or all out)?
# draven

Installing Armadillo library:

```
sudo dnf install armadillo
sudo dnf install armadillo-devel
sudo dnf install vtk
```

Using Conan to check for Boost packages. If you are using *oh-my-zsh* you have
to which back to bash with

```
exec bash
```

then run the following:

```
conan search Boost* -r=conan-center
conan search Boost* -r=conan-transit
```

Adding the beta community conan repo: https://github.com/conan-io/conan/issues/2087

```
conan remote add community https://api.bintray.com/conan/conan-community/conan
conan search Boost* -r=community
```

If installing Boost.Filesystem does not work with Conan, then you have to
manually install it:

```
sudo dnf install boost-filesystem
```
